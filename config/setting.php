<?php

return [

    'register' => [

        '0' => '仮',
        '1' => '登録',
        '2' => '停止',
        '8' => '否認',
        '9' => '退会',
    ],
    'upfile' => [

        '0' => '未処理',
        '1' => '受付済',
    ],

    //'maxupfilesize' => 2097152,
	// 一旦これで様子見
	'upfilesize' => 5242880,
	'maxupfilesize' => 5242880 * 10,
	
	// アップロード許可拡張子一覧（PDF + 画像形式）
	// 拡張子参考：http://www.tohoho-web.com/ex/draft/extension.htm
	"extensionlist" => [
		"pdf",
		"gif",
		"jpg",
		"jpeg",
		"jpe",
		"jfif",
		"png",
		"bmp",
		"dib",
		"rle",
		"ico",
		"ai",
		"art",
		"cam",
		"cdr",
		"cgm",
		"cmp",
		"dpx",
		"fal",
		"q0",
		"fpx",
		"j6i",
		"mac",
		"mag",
		"maki",
		"mng",
		"pcd",
		"pct",
		"pic",
		"pict",
		"pcx",
		"pmp",
		"pnm",
		"psd",
		"ras",
		"sj1",
		"tif",
		"tiff",
		"nsk",
		"tga",
		"wmf",
		"wpg",
		"xbm",
		"xpm"
	]
];
