
function userRegister(mode,id){
	let msg = "";
	switch (mode) {
		case "register":
			msg = "登録します。よろしいですか？";
			break;
		case "reject":
			msg = "登録を否認します。よろしいですか？";
			break;
		default :
			msg = "よろしいですか？";
			break;
	}
    if(!window.confirm(msg)){
        return;
    }
    $('form#list [name="mode"]').val(mode);
    $('form#list [name="userId"]').val(id);
    $("form#list").submit(); //フォーム実行
}

// ユーザー削除
// userlist.jsと関数重複
//function alertDel(uid){
//    if(!window.confirm("本当に削除してよろしいですか？")){
//        return;
//    }
//    $('form#list [name="userId"]').val(uid);
//    $('form#list [name="mode"]').val("delete");
//    $('form#list').submit();
//}
