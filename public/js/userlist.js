    // ユーザー論理削除
    function alertDel(id){
        if(!window.confirm("ユーザーを停止してよろしいですか？")){
            return;
        }
        $('form#list [name="userId"]').val(id);
        $('form#list').attr('action',"userlist");
        $('form#list').find("input[name=mode]").val("delete");
        $('form#list').submit();
    }
    // ユーザー物理削除
    function alertErase(id){
        if(!window.confirm("ユーザーデータが全て削除され、復元はできません。\n本当に削除してよろしいですか？")){
            return;
        }
        $('form#list [name="userId"]').val(id);
        $('form#list').attr('action',"userlist");
        //$('form#list').find("input[name=mode]").val("erase");
        $('form#list [name="mode"]').val("erase");
        $('form#list').submit();
    }
