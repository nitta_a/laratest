$(function () {
	const _head = document.getElementById("vacancy-head");
	const _padd = 15;
	const _width = _head.clientWidth - (_padd * 2);
	_head.style.left = (_width < document.body.clientWidth) ? "auto" : ((window.pageXOffset - _padd) * -1 + "px");
	if (window.innerWidth < 768) {
		$("body").addClass("sp-mode");
	} else {
		$("body").removeClass("sp-mode");
	}
	$(window).on({
		"scroll": function () {
			if (document.body.clientWidth <= _width && !$("body").hasClass("sp-mode")) {
				_head.style.left = (window.pageXOffset - _padd) * -1 + "px";
			}
		},
		"resize": function () {
			if (window.innerWidth < 768) {
				$("body").addClass("sp-mode");
			} else {
				$("body").removeClass("sp-mode");
			}
			if (document.body.clientWidth <= _width && !$("body").hasClass("sp-mode")) {
				_head.style.left = (window.pageXOffset - _padd) * -1 + "px";
			} else {
				_head.style.left = "auto";
			}
		}
	});

	// 行の選択
	$("#vacancy-body").on("click", "tr", function (e) {
		if ($(e.target).hasClass("nairan-chk")) {
			return true;
		}
		if ($(e.target).hasClass("roomdoc")) {
			return true;
		}
		
		if ($(this).hasClass("selected")) {
			$(this).removeClass("selected");
			$(this).find("input.nairan-chk").prop("checked", false);
		} else {
			$(this).addClass("selected");
			$(this).find("input.nairan-chk").prop("checked", true);
		}
	});
	// チェックボックス
	$("#vacancy-body").on("click", "input.nairan-chk", function () {
		if ($(this).prop("checked")) {
			$(this).parents("tr").addClass("selected");
		} else {
			$(this).parents("tr").removeClass("selected");
		}
	});
	
	// 内覧方法の確認（モーダル表示）
	$("#nairan-open").on("click", function () {
		if ($("input.nairan-chk:checked").length === 0) {
			$("#nairan-open-error").removeClass("hide");
			return true;
		} else {
			$("#nairan-open-error").addClass("hide");
		}
		
		$("#vacancy-modal").modal("show");
		$(window).trigger("resize");
	});
	
	$("#vacancy-modal").on("hide.bs.modal", function () {
		if (!$("#nairan-view").hasClass("hide")) {
			$("#vacancy-body").find("tr.selected").removeClass("selected");
			$("#vacancy-body").find("input.nairan-chk:checked").prop("checked", false);
		}
		
		// モーダル内の各値リセット
		$("#vacancy-modal").find("input.form-control").val("");
		$("#vacancy-modal").find("div.clear, tbody.clear").html("");
		$("#modal-footer").find("button").prop("disabled", false);
		$("#nairan-input").removeClass("hide");
		$("#nairan-view, #nairan-error").addClass("hide");
	});
	
	function inputCheck () {
		const _staff = document.getElementById("staff").value;
		const _tel = document.getElementById("tel").value;
		let text = "";
		
		if ($.trim(_staff) === "") {
			text += "<strong>ご担当者名は必須です</strong>";
		}
		if ($.trim(_tel) === "") {
			text += "<strong>電話番号は必須です</strong>";
		}
		
		$("#errors").html(text);
		return (text === "");
	}
	
	// 内覧方法表示
	$("#getnairan").on("click", function () {
		let _bseq = [];
		let _rseq = [];
		
		if (!inputCheck()) {
			return false;
		}
		
		if ($("input.nairan-chk:checked").length === 0) {
			return true;
		} else {
			$("input.nairan-chk:checked").each(function (i, ele) {
				_bseq.push($(ele).data("bseq"));
				_rseq.push($(ele).data("rseq"));
			});
		}
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: 'nairan',
			dataType: 'text',
			data: {
				tel: $("#tel").val(),
				bseq: _bseq,
				rseq: _rseq,
				staff: $("#staff").val()
			}
		}).done(function (results) {
			// 通信成功時の処理
			const obj = JSON.parse(results);
			const errors = obj["errors"];
			let text = "";
			if (errors !== null && Array.isArray(errors)) {
				errors.forEach(function (value) {
					text += "<strong>" + value + "</strong><br>";
				});
				$('#errors').html(text);
			} else {
				$.each(obj, function (i, r) {
					
					text += '<tr>';
					text += '<td>' + r.bname;
					if ($.trim(r.evno) !== "") {
						text += '<div class="evno-ttl">EV用キーの保管場所</div>';
						text += '<div class="evno">' + $.trim(r.evno) + '</div>';
					}
					text += '</td>';
					text += '<td>' + $.trim(r.alno) + '</td>';
					text += '<td>';
					switch (r.shubetu) {
						case "010":
							text += '<span>' + $.trim(r.jno1) + '</span>';
							text += '<span>' + $.trim(r.jno2) + '</span>';
							break;
						case "020":
							text += '<span>' + $.trim(r.tno1) + '</span>';
							text += '<span>' + $.trim(r.tno2) + '</span>';
							break;
					}
					text += '</td>';
					text += '</tr>';
					text += '';
				});
				
				$("#nairan-input").addClass("hide");
				$("#nairan-view").removeClass("hide").find("tbody").html(text);
				$("#modal-footer").find("button").prop("disabled", true);
			}
		}).fail(function (err) {
			// 通信失敗時の処理
			let text = "";
			text += '<p class="ml15">内覧方法のデータ取得処理でエラーが発生しました。</p>';
			text += '<p class="ml15">お手数ですが、下記よりお問合せ下さい。</p>';
			text += '<p class="ml15">株式会社トータルクリエーションズ</p>';
			text += '<p class="ml15">【 大阪 】 TEL：06-6242-6664　FAX：06-6242-6673</p>';
			$("#nairan-input").addClass("hide");
			$("#nairan-error").removeClass("hide").html(text);
			$("#modal-footer").find("button").prop("disabled", true);
		});
	})

	// 資料の表示
//    $("#vacancy-body").on("click", "button.image", function(){
//        var date = new Date();
//        var t = date.getTime();
//        window.open(this.value + "?" + t, "image_win" + t, "width=1170,height=827,scrollbars=yes,resizable=yes");
//    });
});
