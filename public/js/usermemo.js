    function usermemoUpd(id,admin) {
      $.ajaxSetup({
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
      });
      $.ajax({
        type: 'POST',
        url: 'getUsermemoList', // url: は読み込むURLを表す
        dataType: 'text', // 読み込むデータの種類を記入
        data: { users_id: id }
      }).done(function (results) {
        // 通信成功時の処理
        var obj = JSON.parse(results);
        // 返ってきたデータの表示
        $('#result').empty();  // 初期化
        var $result = $('#result');
        var hiduke=new Date();
        var today = hiduke.getFullYear() + "-" + ( '00' + (hiduke.getMonth()+1)).slice(-2) + "-" + ('00' + hiduke.getDate()).slice(-2);
        // 追加用入力欄
        //$result.append('<div class="memodate">' + today + "</div>");
        //$result.append('<div class="memouser">' + admin + "</div>");
        //$result.append('<textarea name="upusermemo[' + id + ']" class="w65 lmg5" rows=4 class="lmg5"></textarea>');
        //$result.append('<input type="button" value="追加" class="rmg5" onclick="usermemoAdd(' + id + ' )">');

        for (var i =0; i<obj.length; i++) {
          $result.append('<div class="memodate">' + obj[i].rdate + "</div>");
          $result.append('<div class="memouser">' + obj[i].t_user + "</div>");
          $result.append('<textarea name="upusermemo[' + obj[i].id + ']" class="w65 lmg5" rows=4 class="lmg5">' + obj[i].memo + "</textarea>");
          $result.append('<input type="button" value="修正" class="rmg5" onclick="usermemoUpdate(' + obj[i].id + ' )">');
          $result.append('<input type="button" value="削除" class="rmg5" onclick="usermemoDelete(' + obj[i].id + ' )">');
        }
        $('#modalForm').modal();
      }).fail(function (err) {
        // 通信失敗時の処理
        alert('ファイルの取得に失敗しました。');
      });
    }

    function usermemoUpdate(id) {
        //var tfm = $('form#usermemoUpd');
        var memo = document.getElementsByName('upusermemo[' + id + ']');
        if(memo[0].value == ""){
            alert('備考を入力してください');
            return false;
        }
        $('#usermemoUpd input[name="memo_id"]').val(id);
        $('#usermemoUpd input[name="mode"]').val('memoupd');
        $('form#usermemoUpd').submit();
        $("#overlay").fadeIn(500); //二度押しを防ぐloading表示
        setTimeout(function(){
          $("#overlay").fadeOut(500);
        },10000);
    }
    function usermemoDelete(id) {
        $('#usermemoUpd input[name="memo_id"]').val(id);
        $('#usermemoUpd input[name="mode"]').val('memodel');
        $('form#usermemoUpd').submit();
        $("#overlay").fadeIn(500); //二度押しを防ぐloading表示
        setTimeout(function(){
          $("#overlay").fadeOut(500);
        },10000);
    }
    function usermemoAdd(id){
        var memo = document.getElementsByName('usermemo[' + id + ']');
        if(memo[0].value == ""){
            alert('備考を入力してください');
            return false;
        }
        $('#list input[name="userId"]').val(id);
        $('#list input[name="mode"]').val('memoadd');
        $('form#list').submit();
        
        //document.getElementsByName('usermemo[' + id + ']').value = ""; //初期化
        $("#overlay").fadeIn(500); //二度押しを防ぐloading表示
        setTimeout(function(){
          $("#overlay").fadeOut(500);
        },10000);
    }
