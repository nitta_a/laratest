    function fileDownload(id){
        $('form#uplist [name="mode"]').val('download');
        $('form#uplist [name="fileId"]').val(id);
        $("form#uplist").attr('action','download');
        $("form#uplist").attr('target','_self');
        $("form#uplist").submit(); //フォーム実行
    }
    function flgChange(){
        $('form#uplist [name="mode"]').val('flgchange');
        $("form#uplist").attr('action','upfilelist');
		$("form#uplist").attr('target','_self');
        $("form#uplist").submit(); //フォーム実行
    }
    function fileDel(id){
        if(!window.confirm("ファイルを削除してよろしいですか？")){
            return;
        }
        $('form#uplist').find("input[name=mode]").val("delete");
        $('form#uplist [name="fileId"]').val(id);
        $('form#uplist').attr('action',"fileDelete");
        $('form#uplist').submit();
    }
    function allfileDelete(){
        if(!window.confirm("ファイルを削除してよろしいですか？")){
            return;
        }
        $('form#uplist [name="mode"]').val('allfileDelete');
        $("form#uplist").attr('action','allfiledel');
        $("form#uplist").submit(); //フォーム実行
    }
