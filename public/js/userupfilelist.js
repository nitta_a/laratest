    function upfile(){
		if ($("#preview_area").find(".dz-preview.dz-success").length === 0) {
            alert("アップロードファイルが選択されていません");
            return false;
		}
		
		let namecheck = true;
		let $dzname;
		$("#preview_area").find(".dz-preview.dz-success").each(function (i) {
			$dzname = $(this).find("[data-dz-name]");
			if ($dzname.data("dz-name") === undefined || $dzname.data("dz-name") === "") {
				namecheck = false;
				return false;
			}
			$("<input>")
					.appendTo($("form#fileUpload"))
					.attr("type", "hidden")
					.attr("name", "filenames[" + i + "]")
					.val($dzname.text());
			$("<input>")
					.appendTo($("form#fileUpload"))
					.attr("type", "hidden")
					.attr("name", "hashnames[" + i + "]")
					.val($dzname.data("dz-name"));
		});
		if (namecheck === false) {
            alert("アップロード処理に失敗しました");
            return false;
		}
		
//        var names = [];
//        $('.dz-filename').each(function(i) {
//            names[i] = this.textContent ;
//		});
//        if(names.length == 0){
//            alert('ファイルがドラッグ＆ドロップされていません');
//            return false;
//        }
//        $('input[name="filename"]').val(names) ;
        $('form#fileUpload').submit();
        $("#overlay").fadeIn(500); //二度押しを防ぐloading表示
        setTimeout(function(){
            $("#overlay").fadeOut(500);
        },3000);

    }
    function fileDownload(id){
        $('form#uplist [name="mode"]').val('download');
        $('form#uplist [name="fileId"]').val(id);
        $("form#uplist").attr('action','download');
        $("form#uplist").attr('target','_self');
        $("form#uplist").submit(); //フォーム実行
    }
    function fileDel(id){
        if(!window.confirm("ファイルを削除してよろしいですか？")){
            return;
        }
        $('form#uplist').find("input[name=mode]").val("delete");
        $('form#uplist [name="fileId"]').val(id);
        $('form#uplist').attr('action',"fileDelete");
		$("form#uplist").attr('target','_self');
        $('form#uplist').submit();
    }
    function allfileDelete(){
		if(!window.confirm("ファイルを削除してよろしいですか？")){
            return;
        }
        $('form#uplist [name="mode"]').val('allfileDelete');
        $("form#uplist").attr('action','allfiledel');
		$("form#uplist").attr('target','_self');
        $("form#uplist").submit(); //フォーム実行
    }
