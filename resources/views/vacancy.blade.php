<?php

/**
 * 英数字の全角→半角変換
 */
function a($str) {
	return mb_convert_kana($str, "a");
}

/**
 * 文字列(数字+文字)の数字部分フォーマット
 */
function bill($str) {
	$a = a($str);
	$b = str_replace(",", "", $a);
	$bill = preg_replace_callback(
			"/[0-9]++/",
			function($m) {
		return number_format($m[0]);
	}, $b);
	return $bill;
}
?>
@extends('layouts.user.app')

@section('content')
<div class="container vacancy">
	<section id="vacancy-head">
		<div class="btn-box">
			<a class="btn-cc btn-c py-1 btn @if ($sort === 'bname') disabled @endif" href="{{route('vacancy_sort', 'bname')}}">並べ替え：物件名</a>
			<a class="btn-cc btn-c py-1 btn @if ($sort === 'address') disabled @endif" href="{{route('vacancy_sort', 'address')}}">並べ替え：住所</a>
			<button id="nairan-open" class="btn-cc btn-c py-1 btn">内覧方法の確認</button>
			<span id="nairan-open-error" class="hide">※ 内覧予定の部屋の行を選択してください。</span>
		</div>
		<div class="msg-box">
			<div class="cp-view">
				<div class="block"></div>
				キャンペーン適用後の内容
			</div>
			<time>
				【{{ date("Y/m/d H:00 現在", strtotime($synctime)) }}】
			</time>
		</div>
		<table class="vacancy-tbl">
			<colgroup>
				<col class="bname">
				<col class="adrs">
				<col class="nairan">
				<col class="njokyo">
				<col class="rno">
				<col class="rpath">
				<col class="madori">
				<col class="rtype">
				<col class="area">
				<col class="tinryo">
				<col class="kyoeki">
				<col class="suido">
				<col class="hosho">
				<col class="reikin">
				<col class="kokoku">
				<col class="genkyo">
				<col class="taikyo">
				<col class="nkano">
				<col class="biko">
			</colgroup>
			<thead>
				<tr>
					<th>物件名</th>
					<th>住所</th>
					<th>内覧</th>
					<th>申込</th>
					<th>号室</th>
					<th>資料</th>
					<th>間取り</th>
					<th>タイプ</th>
					<th>面積</th>
					<th>賃料</th>
					<th>共益費</th>
					<th>水道代</th>
					<th>保証金</th>
					<th>礼金</th>
					<th>広告料</th>
					<th>現況</th>
					<th>退去予定</th>
					<th>入居可能</th>
					<th>備考</th>
				</tr>
			</thead>
		</table>
	</section>
	<section id="vacancy-body">
		<table class="vacancy-tbl">
			<colgroup>
				<col class="bname">
				<col class="adrs">
				<col class="nairan">
				<col class="njokyo">
				<col class="rno">
				<col class="rpath">
				<col class="madori">
				<col class="rtype">
				<col class="area">
				<col class="tinryo">
				<col class="kyoeki">
				<col class="suido">
				<col class="hosho">
				<col class="reikin">
				<col class="kokoku">
				<col class="genkyo">
				<col class="taikyo">
				<col class="nkano">
				<col class="biko">
			</colgroup>
			<tbody>
				<?php
				$bseq = null;
				?>
				@foreach ([$kyoju, $zigyo] as $all)
				@foreach ($all as $row)
				@if ($loop->parent->index === 1 && $loop->first)
				<tr class="header">
					<th colspan="19">◎募集物件明細（事業用）</th>
				</tr>
				@endif
				<?php
				$sameflg = ($bseq === $row->bseq);
				$bseq = $row->bseq;
				?>
				@if ($sameflg)
				<tr class="same"> @else <tr>
					@endif
					<td class="bname">
						@if (!$sameflg)
						{{$row->bname}}
						@endif
					</td>
					<td class="adrs">
						@if (!$sameflg)
						{{a($row->adrs1.$row->adrs2.$row->adrs3)}}
						@endif
					</td>
					<td class="nairan">
						<input type="checkbox" class="nairan-chk" value="1" data-bseq="{{$row->bseq}}" data-rseq="{{$row->rseq}}" />
					</td>
					<td class="njokyo @if ($row->njokyo !== null) ari @endif">
						@if ($row->njokyo !== null) 有 @endif
					</td>
					<td class="rno">{{a($row->rno)}}</td>
					<td class="rpath">
						@if ((string)$row->rpath !== "")
						<a class="roomdoc" href="{{route('roomdoc', $row->rseq)}}" target="_win{{$row->rseq}}">資</a>
						@endif
					</td>
					<td class="madori">{{a($row->madori)}}</td>
					<td class="rtype">{{a($row->rtype)}}</td>
					<td class="area">{{$row->area}}</td>
					<td class="tinryo @if ($row->ticp === 1) cp @endif">{{bill($row->tinryo)}}</td>
					<td class="kyoeki @if ($row->kyocp === 1) cp @endif">{{bill($row->kyoeki)}}</td>
					<td class="suido">{{bill($row->suido)}}</td>
					<td class="hosho @if ($row->hocp === 1) cp @endif">{{bill($row->hosho)}}</td>
					<td class="reikin @if ($row->recp === 1) cp @endif">{{bill($row->reikin)}}</td>
					<td class="kokoku @if ($row->kocp === 1) cp @endif">
						@if (is_numeric($row->kokoku)) {{$row->kokoku}}ヶ月
						@else {{$row->kokoku}} @endif
					</td>
					<td class="genkyo">{{$row->genkyo}}</td>
					<td class="taikyo">
						@if ($row->taiyoteim !== null && $row->taiyoteid !== null)
						{{$row->taiyoteim."月".$row->taiyoteid}}@if (is_numeric($row->taiyoteid))日 @endif
						@endif

					</td>
					<td class="nkano">{{$row->nkanou}}</td>
					<td class="biko">{{a($row->biko)}}</td>
				</tr>
				@endforeach
				@endforeach
			</tbody>
		</table>
	</section>
	<section class="modal-section">
		<div class="modal fade" id="vacancy-modal" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
					<div class="modal-header">
						<div class="blc">
							<label>内覧方法表示</label>
						</div>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="wrap">
						<div id="nairan-input">
							<p class="ml15">内覧ご担当者様のお名前・電話番号をご入力ください。</p>
							<div class="contact">
								<div class="form-group row">
									<label class="col-md-4 col-form-label text-md-right">ご担当者名</label>
									<div class="col-md-6">
										<input id="staff" type="name" class="form-control" name="staff" value="" required autocomplete="name" autofocus>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-4 col-form-label text-md-right">電話番号</label>
									<div class="col-md-6">
										<input id="tel" type="tel" class="form-control" name="tel" required autocomplete="tel">
									</div>
								</div>
							</div>
							<div id="errors" class="invalid-feedback clear"></div>
						</div>
						
						<div id="nairan-view" class="hide">
							<table class="nairan">
								<colgroup>
									<col class="bname">
									<col class="alno">
									<col class="houhou">
								</colgroup>
								<thead>
									<tr>
										<th class="bname">物件名</th>
										<th class="alno">オートロック開錠番号</th>
										<th class="houhou">内覧方法</th>
									</tr>
								</thead>
								<tbody class="clear"></tbody>
							</table>
							<div class="msg">
								<p>内覧方法についてご不明点等ございましたら、下記よりお問合せ下さい。</p>
								<p>株式会社トータルクリエーションズ</p>
								<p>【 大阪 】 TEL：06-6242-6664　FAX：06-6242-6673</p>
							</div>
						</div>
						
						<div id="nairan-error" class="hide clear"></div>
					</div>
					<div id="modal-footer" class="modal-footer">
						<button type="button" id="getnairan" class="btn btn-primary">内覧方法表示</button>
					</div>
                </div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('vacancy-js')
<script src="{{ asset('js/vacancy.js') }}"></script>
@endsection