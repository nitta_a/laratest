
@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
<!--# @lang('Hello!')-->
<!--# @lang('会員登録のご案内')-->
@endif
@endif

{{-- Intro Lines --}}
@isset($introLines)
@foreach ($introLines as $line)
{{ $line }}

@endforeach
@endisset

{{-- Custom Lines --}}
@isset($customLines)
@foreach ($customLines as $line)
{!! nl2br(e($line)) !!}

@endforeach
@endisset

{{-- Table --}}
@isset($table)
@component('mail::table')
{!! $table !!}
@endcomponent
@endisset

{{-- Action Button --}}
@isset($actionText)
<?php
	if (!isset($level)) {
		$level = 'info';
	}
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@isset($outroLines)
@foreach ($outroLines as $line)
{{ $line }}

@endforeach
@endisset

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
<!-- @lang('Regards'),<br> -->
<!--{{ config('app.name') }}より-->
@endif

{{-- Subcopy --}}
@slot('subcopy')
@isset($actionText)
    <!-- "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser: [:actionURL](:actionURL)', -->
@lang("
「:actionText」ボタンを押しても動かない場合は、次のURLよりアクセスしてください。<br>
[:actionURL](:actionURL)",
[
	'actionText' => $actionText,
	'actionURL' => $actionUrl,
]
)


@endisset
---
@lang("
このメールはシステムより自動配信されています。<br>
心当たりのない場合や、ご不明な点がございましたら、以下よりお問い合わせください。<br>
株式会社トータルクリエーションズ ／ TEL：06-6242-6664
")
@endslot
@endcomponent

