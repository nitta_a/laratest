@extends('layouts.user.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('退会手続き') }}</div>

                <div class="card-body">
                  <span class="">会員の退会手続きを行います。<br>注意事項の記載内容をご確認いただき、パスワードを入力して「退会する」ボタンを押してください。</span><br>
                <div class="card-body">
                    <form method="POST" action="{{ route('withdraw') }}" class="h-adr">
                        @csrf
                        <div class="form-group row attention">
                            <label for="caution" class="col-md-12 col-form-label text-md-center"><strong>注意事項</strong></label>
                            <br>
                              <ul class="mr-3">
                                <li>退会は取り消しができません。</li>
                                <li>退会するとすべての会員向けサービスは、即時にご利用いただけなくなります。</li>
                                <li>退会後、本サイトに記録された会員の情報は一切保持せず削除いたします。</li>
                                <li>再度登録をおこなった場合の再登録による情報の引継ぎは一切行われません。</li>
                              </ul>
                        </div>
                        <div><label><input id="consent" type="checkbox" class=" @error('consent') is-invalid @enderror" name="consent" value="1" required autocomplete="consent" @if( old('consent') == "1") checked @endif>
						上記内容に同意します。</label></div>
                        <div class="form-inline row">
                          <div class="col-md-12"><label">パスワード</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="password"></div>
                        </div><br>
        @if (session('password_error'))
          <div class="container mt-2">
            <div class="alert alert-danger">
              {{session('password_error')}}
            </div>
          </div>
        @endif
        @if (session('consent_error'))
          <div class="container mt-2">
            <div class="alert alert-danger">
              {{session('consent_error')}}
            </div>
          </div>
        @endif

                        <div class="form-group text-md-center">
                                <button type="submit" class="btn btn-primary w-50">
                                    {{ __('退会する') }}
                                </button>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
