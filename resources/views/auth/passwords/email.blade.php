@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('ID・パスワードをお忘れの方') }}</div>

                <div class="card-body">
                    @if (session('status'))
					<div class="alert alert-success" role="alert">
						{{ session('status') }}
					</div>
                    @else
                    ご登録のメールアドレスに、パスワードリセット用URLをお送りしますので、メールアドレスを入力して送信してください。<br><br>
					<ul>
						<li>
							<p>パスワードを忘れた場合</p>
							<p>
								お送りしたURLより新しいパスワードを設定してください。
							</p>
						<li>
							<p>IDを忘れた場合</p>
							<p>
								URLよりアクセスいただいた画面にIDが表示されております。<br>
								パスワードリセットは任意です。
							</p>
						</li>
					</ul>

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row pt-4 pb-2">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('メールアドレス') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary w-50">
                                    {{ __('送信') }}
                                </button>
                            </div>
                        </div>
                    </form>
					@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
