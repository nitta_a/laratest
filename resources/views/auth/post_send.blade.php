@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center col-sm-12">
      <div class="txtcenter">
        <ul class="step">
            <li class="stepfalse col-md-auto">STEP1<br>メールアドレスの入力</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="stepfalse col-md-auto">STEP2<br>ユーザー情報登録</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="steptrue col-md-auto">STEP3<br>仮登録完了</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="stepfalse col-md-auto">STEP4<br>審査後、登録完了</li>
        </ul>
      </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('ユーザー情報登録') }}</div>

                <div class="card-body">
仮登録が完了しました。<br>
登録情報の審査後にID・PWが発行され、ご登録のメールアドレス宛に通知されますので今しばらくお待ちください。<br><br>

            @if(Session::has('userid'))
                    <form method="POST" action="./post_register">
                        @csrf

                        <div class="form-group row">
                            <label for="kname" class="col-md-3 col-form-label text-md-right">企業名</label>

                            <div class="col-md-8">
                                <input id="kname" type="text" class="form-control-plaintext fcsnon" name="kname" value="{{ $param['kname'] }}" required autocomplete="kname" autofocus readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="yname" class="col-md-3 col-form-label text-md-right">屋号</label>

                            <div class="col-md-8">
                                <input id="yname" type="text" class="form-control-plaintext fcsnon" name="yname" value="{{ $param['yname'] }}" autocomplete="yname" autofocus readonly>

                                @error('yname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sname" class="col-md-3 col-form-label text-md-right">支店名</label>

                            <div class="col-md-8">
                                <input id="sname" type="text" class="form-control-plaintext fcsnon" name="sname" value="{{ $param['sname'] }}" required autocomplete="sname" autofocus readonly>

                                @error('sname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="license" class="col-md-3 col-form-label text-md-right">宅建免許番号</label>

                            <div class="col-md-8">
                                <input id="license" type="text" class="form-control-plaintext fcsnon" name="license" value="{{ $param['license'] }}" required autocomplete="license" readonly>

                                @error('license')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="zip" class="col-md-3 col-form-label text-md-right">郵便番号</label>

                            <div class="form-inline col-md-8">
                                <input id="zip" type="text" class="col-md-4 form-control-plaintext fcsnon" name="zip" value="{{ $param['zip1'] }}-{{ $param['zip2'] }}" required autocomplete="zip" readonly>

                                @error('zip')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <!--input id="zip_search" type="button" class="col-md-6 form-control btn btn-primary" name="zip_search" style="display: inline-block" required autocomplete="zip_search" value="郵便番号から住所入力"-->
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address1" class="col-md-3 col-form-label text-md-right">住所１</label>

                            <div class="col-md-8">
                                <input id="address1" type="text" class="form-control-plaintext fcsnon" value="{{ $param['address1'] }}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address2" class="col-md-3 col-form-label text-md-right">住所２</label>

                            <div class="col-md-8">
                                <input id="address2" type="text" class="form-control-plaintext fcsnon" name="address2" value="{{ $param['address2'] }}" required autocomplete="address2" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address3" class="col-md-3 col-form-label text-md-right">住所３</label>

                            <div class="col-md-8">
                                <input id="address3" type="text" class="form-control-plaintext fcsnon" name="address3" value="{{ $param['address3'] }}" autocomplete="address3" readonly>

                                @error('address3')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tel" class="col-md-3 col-form-label text-md-right">電話番号</label>

                            <div class="col-md-8">
                                <input id="tel" type="text" class="form-control-plaintext fcsnon" name="tel" value="{{ $param['tel'] }}" required autocomplete="tel" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fax" class="col-md-3 col-form-label text-md-right">FAX番号</label>

                            <div class="col-md-8">
                                <input id="fax" type="text" class="form-control-plaintext fcsnon" name="fax" value="{{ $param['fax'] }}" required autocomplete="fax" readonly>

                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3  text-md-right">
                                <!--<input id="kiyaku" type="checkbox" class=" @error('kiyaku') is-invalid @enderror" name="kiyaku" value="{{ $param['kiyaku'] }}" required autocomplete="kiyaku" checked readonly>-->
                            </div>

                            <div class="col-md-8">
                                <a href="{{ url('/terms') }}" name="kiyaku" target="_blank">利用規約</a>に同意します

                            </div>
                        </div>

                    </form>
            @endif
                  <div class="row">
                    <div class="col-sm-12 text-center">
                      <input type="button" name="close" class="form-control btn btn-primary" style="width:200px;" value="HOMEへ戻る" onclick="linkurl('home')"><br><br>
                    </div>
                  </div>


                </div>

            </div>
        </div>
    </div>
</div>
@endsection
