@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center col-sm-12">
      <div class="txtcenter">
        <ul class="step">
            <li class="steptrue col-md-auto">STEP1<br>メールアドレスの入力</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="stepfalse col-md-auto">STEP2<br>ユーザー情報登録</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="stepfalse col-md-auto">STEP3<br>仮登録完了</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="stepfalse col-md-auto">STEP4<br>審査後、登録完了</li>
        </ul>
      </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="regist">
                <div class="card-body">
ご入力いただいたメールアドレスに会員登録用のURLをお送りしました。<br>
URLの有効期限内（{{config('auth.verification.expire', 60)}}分以内）に認証を行い、ユーザー情報のご登録をお願いいたします。<br><br>

しばらく経ってもメールが届かない場合<br>
・迷惑メールに振り分けされていないかどうかご確認ください。<br>
・ご登録メールアドレスに誤りがないかご確認のうえ、再度お試しください。
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
