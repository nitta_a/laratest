    <div class="main_inner form">
        <h1 class="alignCenter">OK</h1>
        <p class="alignCenter">
            メールアドレスの認証が完了しました。
            <br>
            名前とパスワードを設定してください。
        </p>
        {!! Form::open(['route' => ['registered', $token], 'method' => 'post']) !!}

        {{ csrf_field() }}

        @if(session('message') OR isset($message))
            <div class="success m-b2">
                {!! session('message') ? session('message') : $message !!}
            </div>
        @endif

        @if(session('error'))
            <div class="end m-b2">
                {!! session('error') !!}
            </div>
        @endif

        @if (count($errors) > 0)
            <ul class="end">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <p class="bold m-t3">名前</p>
        <p>{{ Form::text('name', null, ['placeholder' => '名前', 'required']) }}</p>

        <p class="bold m-t3">パスワード</p>
        <p>{{ Form::password('password', ['placeholder' => 'パスワード', 'required']) }}</p>

        <p class="bold m-t3">確認用パスワード</p>
        <p>{{ Form::password('password_confirmation', ['placeholder' => '確認用パスワード', 'required']) }}</p>

        <div class="m-t3">{{ Form::submit('登録する', ['class' => 'btn']) }}</div>

        {!! Form::close() !!}
    </div>