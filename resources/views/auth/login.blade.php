@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="login">
                <div class="login-header h5">{{ config('app.sname').__(' ログイン') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

						<div class="login-form">
							<div class="form-group row">
								<label for="uid" class="col-md-3 col-form-label text-md-right">ID</label>

								<div class="col-md-7">
									<input id="uid" type="uid" class="form-control @error('uid') is-invalid @enderror" name="uid" value="{{ old('uid') }}" required autocomplete="uid" autofocus>

									@error('uid')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
									@enderror
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-3 col-form-label text-md-right">PW</label>

								<div class="col-md-7">
									<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

									@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
									@enderror
								</div>
							</div>
						</div>
                        <div class="form-group row">
                            <div class="col-md-7 offset-md-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

										   <label class="form-check-label" for="remember">
                                        {{ __('次回ログインID・PWの入力を省略') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-7 offset-md-3">
                                <button type="submit" class="btn btn-primary w-100">
                                    {{ __('ログイン') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="regist">
                <div class="login-body">
					<div class="form-group row mb-0 p-3">
						<div class="col-sm-12">
							<div class="col-sm-7 offset-sm-1 float-left">
								{!! __('会員登録をされていない仲介業者様は、こちらよりお申込み下さい。登録には宅建免許番号が必要です。') !!}
							</div>
							<div class="col-sm-4 float-right">
								<button type="button" class="btn btn-primary w-100" onclick="linkurl('{{ route("register") }}')">
									{{ __('新規会員登録') }}
								</button>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="regist">
                <div class="login-body">
					<div class="form-group row mb-0 p-3">
						<div class="col-sm-12">
							<div class="col-sm-7 offset-sm-1 float-left col-form-label">
								{!! __('ID・PWをお忘れの方はこちら') !!}
							</div>
							<div class="col-sm-4 float-right">
								@if (Route::has('password.request'))
								<button type="button" class="btn btn-primary w-100" onclick="linkurl('{{ route("password.request") }}')">
									{{ __('PWリセット') }}
								</button>
								@endif
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
