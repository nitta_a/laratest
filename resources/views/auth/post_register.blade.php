@extends('layouts.app')

@section('script')
<script src="{{ asset('js/address.js') }}" type="text/javascript"></script>
@endsection

@section('content')
<div class="container">
    <div class="d-flex justify-content-center col-sm-12">
      <div class="txtcenter">
        <ul class="step">
            <li class="stepfalse col-md-auto">STEP1<br>メールアドレスの入力</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="steptrue col-md-auto">STEP2<br>ユーザー情報登録</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="stepfalse col-md-auto">STEP3<br>仮登録完了</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="stepfalse col-md-auto">STEP4<br>審査後、登録完了</li>
        </ul>
      </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('ユーザー情報登録') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('post_confirm') }}" class="h-adr">
                        @csrf

                        <input type="hidden" name="mid" value="{{ $mid }}">
                        <input type="hidden" name="hash" value="{{ $hash }}">
                        <div class="form-group row">
                            <label for="kname" class="col-md-3 col-form-label text-md-right">ご登録メールアドレス</label>

                            <div class="col-md-8">
                                <input id="email" type="text" class="form-control-plaintext" name="email" value="{{ $email }}" required autocomplete="email" autofocus readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kname" class="col-md-3 col-form-label text-md-right">企業名</label>

                            <div class="col-md-8">
                                <input id="kname" type="text" class="form-control @error('kname') is-invalid @enderror" name="kname" value="{{ old('kname') }}" required autocomplete="kname" autofocus>

                                @error('kname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="yname" class="col-md-3 col-form-label text-md-right">屋号</label>

                            <div class="col-md-8">
                                <input id="yname" type="text" class="form-control @error('yname') is-invalid @enderror" name="yname" value="{{ old('yname') }}" autocomplete="yname" autofocus>

                                @error('yname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sname" class="col-md-3 col-form-label text-md-right">支店名</label>

                            <div class="col-md-8">
                                <input id="sname" type="text" class="form-control @error('sname') is-invalid @enderror" name="sname" value="{{ old('sname') }}" required autocomplete="sname" autofocus>

                                @error('sname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="license" class="col-md-3 col-form-label text-md-right">宅建免許番号</label>

                            <div class="col-md-8">
                                <input id="license" type="text" class="form-control @error('license') is-invalid @enderror" name="license" value="{{ old('license') }}" required autocomplete="license">

                                @error('license')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="zip" class="col-md-3 col-form-label text-md-right">郵便番号</label>
                            <span class="p-country-name" style="display:none;">Japan</span>
                            <div class="form-inline col-md-8">
                                <input id="zip" type="text" class="p-postal-code col-md-4 form-control @error('zip') is-invalid @enderror" name="zip" value="{{ old('zip') }}" required autocomplete="zip" size="8" maxlength="8">


                                <input id="zip_search" type="button" class="col-md-7 form-control btn btn-primary" name="zip_search" style="display: inline-block" required autocomplete="zip_search" value="郵便番号から住所入力" onclick="addreset()">
                                @error('zip')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address1" class="col-md-3 col-form-label text-md-right">住所１</label>

                            <div class="col-md-8">
                                <!--input id="address1" type="text" class="p-region form-control @error('address1') is-invalid @enderror" name="address1" value="{{ old('address1') }}" required autocomplete="address1"-->
                                
                                <select id="address1" name="address1" required="required" class="p-region-id form-control @error('address1') is-invalid @enderror" value="{{ old('address1') }}" required autocomplete="address1" style="max-width:180px"><option value="">都道府県を選択</option>
                                @foreach(config('area.address1') as $key => $value)
                                <option value="{{ $key }}"@if( old('address1') == $key) selected @endif>{{ $value }}</option>
                                @endforeach
                                </select>

                                @error('address1')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address2" class="col-md-3 col-form-label text-md-right">住所２</label>

                            <div class="col-md-8">
                                <input id="address2" type="text" class="p-locality p-street-address form-control @error('address2') is-invalid @enderror" name="address2" value="{{ old('address2') }}" required autocomplete="address2" placeholder="市区町村名">

                                @error('address2')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address3" class="col-md-3 col-form-label text-md-right">住所３</label>

                            <div class="col-md-8">
                                <input id="address3" type="text" class="p-extended-address form-control @error('address3') is-invalid @enderror" name="address3" value="{{ old('address3') }}" autocomplete="address3" placeholder="番地・ビル名">

                                @error('address3')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tel" class="col-md-3 col-form-label text-md-right">電話番号</label>

                            <div class="col-md-8">
                                <input id="tel" type="text" class="form-control @error('tel') is-invalid @enderror" name="tel" value="{{ old('tel') }}" required autocomplete="tel">

                                @error('tel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fax" class="col-md-3 col-form-label text-md-right">FAX番号</label>

                            <div class="col-md-8">
                                <input id="fax" type="text" class="form-control @error('fax') is-invalid @enderror" name="fax" value="{{ old('fax') }}" required autocomplete="fax">

                                @error('fax')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3  text-md-right">
                                <input id="kiyaku" type="checkbox" class=" @error('kiyaku') is-invalid @enderror" name="kiyaku" value="1" required autocomplete="kiyaku" @if( old('kiyaku') == 1) checked @endif >
                            </div>

                            <div class="col-md-8">
                                <a href="{{ url('/terms') }}" name="kiyaku" target="_blank">利用規約</a>に同意してお進みください

                                @error('fax')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary" style="width:180px">
                                    同意して次へ
                                </button>
                            </div>
                        </div><br>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="button" class="btn btn-secondary" style="width:180px">
                                    同意しない
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
