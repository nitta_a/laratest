@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center">
		<div class="txtcenter">
			<ul class="step">
				<li class="steptrue col-md-auto">STEP1<br>メールアドレスの入力</li>
				<li class="next"><div class="arrow"></div></li>
				<li class="stepfalse col-md-auto">STEP2<br>ユーザー情報登録</li>
				<li class="next"><div class="arrow"></div></li>
				<li class="stepfalse col-md-auto">STEP3<br>仮登録完了</li>
				<li class="next"><div class="arrow"></div></li>
				<li class="stepfalse col-md-auto">STEP4<br>審査後、登録完了</li>
			</ul>
			<!--p>会員登録用URLのご案内メールをお送りしますので、メールアドレスの送信をお願いいたします。</p-->
		</div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="regist">
                <div class="card-body">会員登録用URLのご案内メールをお送りしますので、メールアドレスの送信をお願いいたします。</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('メールアドレス') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
                                @enderror
                            </div>
                            <br><br>
                            <label for="email" class="col-md-4 col-form-label text-md-right"></label>
                            <div class="col-md-6">
                                <input id="email_confirm" type="email_confirm" class="form-control @error('email_confirm') is-invalid @enderror" name="email_confirm" value="{{ old('email_confirm') }}" required autocomplete="email_confirm" placeholder="確認のためもう一度入力してください">

                                @error('email_confirm')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary w-50">
                                    {{ __('送信') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
