@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center col-sm-12">
      <div class="txtcenter">
        <ul class="step">
            <li class="stepfalse col-md-auto">STEP1<br>メールアドレスの入力</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="steptrue col-md-auto">STEP2<br>ユーザー情報登録</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="stepfalse col-md-auto">STEP3<br>仮登録完了</li>
            <li class="next"><div class="arrow"></div></li>
            <li class="stepfalse col-md-auto">STEP4<br>審査後、登録完了</li>
        </ul>
      </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('ユーザー情報登録') }}</div>

                <div class="card-body">
                    <form method="POST" action="./post_register">
                        @csrf

                        <input type="hidden" name="mid" value="{{ $param['mid'] }}">
                        <input type="hidden" name="hash" value="{{ $param['hash'] }}">
                        <div class="form-group row">
                            <label for="kname" class="col-md-3 col-form-label text-md-right">ご登録メールアドレス</label>

                            <div class="col-md-8">
                                <input id="email" type="text" class="form-control-plaintext" name="email" value="{{ $param['email'] }}" required autocomplete="email" autofocus readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kname" class="col-md-3 col-form-label text-md-right">企業名</label>

                            <div class="col-md-8">
                                <input id="kname" type="text" class="form-control-plaintext fcsnon" name="kname" value="{{ $param['kname'] }}" required autocomplete="kname" autofocus readonly>

                                @error('kname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="yname" class="col-md-3 col-form-label text-md-right">屋号</label>

                            <div class="col-md-8">
                                <input id="yname" type="text" class="form-control-plaintext fcsnon" name="yname" value="{{ $param['yname'] }}" autocomplete="yname" autofocus readonly>

                                @error('yname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sname" class="col-md-3 col-form-label text-md-right">支店名</label>

                            <div class="col-md-8">
                                <input id="sname" type="text" class="form-control-plaintext fcsnon" name="sname" value="{{ $param['sname'] }}" required autocomplete="sname" autofocus readonly>

                                @error('sname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="license" class="col-md-3 col-form-label text-md-right">宅建免許番号</label>

                            <div class="col-md-8">
                                <input id="license" type="text" class="form-control-plaintext fcsnon" name="license" value="{{ $param['license'] }}" required autocomplete="license" readonly>

                                @error('license')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="zip" class="col-md-3 col-form-label text-md-right">郵便番号</label>

                            <div class="form-inline col-md-8">
                                <input id="zip" type="text" class="col-md-4 form-control-plaintext fcsnon" name="zip" value="{{ $param['zip'] }}" required autocomplete="zip" readonly>

                                @error('zip')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <!--input id="zip_search" type="button" class="col-md-6 form-control btn btn-primary" name="zip_search" style="display: inline-block" required autocomplete="zip_search" value="郵便番号から住所入力"-->
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address1" class="col-md-3 col-form-label text-md-right">住所１</label>

                            <div class="col-md-8">
                                <input id="address1" type="hidden" name="address1" value="{{ $param['address1'] }}" required autocomplete="address1" readonly>
                                <input id="address1" type="text" class="form-control-plaintext fcsnon" value="{{ config("area.address1.{$param['address1']}") }}" readonly>

                                @error('address1')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address2" class="col-md-3 col-form-label text-md-right">住所２</label>

                            <div class="col-md-8">
                                <input id="address2" type="text" class="form-control-plaintext fcsnon" name="address2" value="{{ $param['address2'] }}" required autocomplete="address2" readonly>

                                @error('address2')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address3" class="col-md-3 col-form-label text-md-right">住所３</label>

                            <div class="col-md-8">
                                <input id="address3" type="text" class="form-control-plaintext fcsnon" name="address3" value="{{ $param['address3'] }}" autocomplete="address3" readonly>

                                @error('address3')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tel" class="col-md-3 col-form-label text-md-right">電話番号</label>

                            <div class="col-md-8">
                                <input id="tel" type="text" class="form-control-plaintext fcsnon" name="tel" value="{{ $param['tel'] }}" required autocomplete="tel" readonly>

                                @error('tel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fax" class="col-md-3 col-form-label text-md-right">FAX番号</label>

                            <div class="col-md-8">
                                <input id="fax" type="text" class="form-control-plaintext fcsnon" name="fax" value="{{ $param['fax'] }}" required autocomplete="fax" readonly>

                                @error('fax')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3  text-md-right">
                                <input id="kiyaku" type="checkbox" class=" @error('kiyaku') is-invalid @enderror" name="kiyaku" value="{{ $param['kiyaku'] }}" required autocomplete="kiyaku" checked readonly>
                            </div>

                            <div class="col-md-8">
                                <a href="{{ url('/terms') }}" name="kiyaku" target="_blank">利用規約</a>に同意します

                                @error('fax')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary" style="width:180px">
                                    送信
                                </button>
                            </div>
                        </div><br>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" name="back" class="btn btn-secondary" style="width:180px" value="true">
                                    戻る
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
