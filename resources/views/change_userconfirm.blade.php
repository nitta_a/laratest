@extends('layouts.user.app')

@section('script')
    <!--script src="https://yubinbango.github.io/yubinbango/yubinbango.js" charset="UTF-8"></script-->
    <!--script src="{{ asset('js/address.js') }}" type="text/javascript"></script-->
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('会員情報の変更') }}</div>

                <div class="card-body">
                @if ( isset($complete) )
                  <span class="">変更が完了しました。</span>
                @else
                この内容で送信いたします。よろしければ送信ボタンを押してください。
                <?php  $complete = "0";?>
                @endif

                    <form method="POST" action="{{ route('changeuserregist') }}" class="h-adr">
                        @csrf

                        <input type="hidden" name="id" value="{{ $id }}">

                        <div class="form-group row">
                            <label for="kname" class="col-md-3 col-form-label text-md-right">企業名</label>

                            <div class="col-md-8">
                                <input id="kname" type="text" class="form-control-plaintext fcsnon" name="kname" value="{{ $user['kname'] }}" required autocomplete="kname" autofocus readonly>

                                @error('kname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="yname" class="col-md-3 col-form-label text-md-right">屋号</label>

                            <div class="col-md-8">
                                <input id="yname" type="text" class="form-control-plaintext fcsnon" name="yname" value="{{ $user['yname'] }}" autocomplete="yname" autofocus readonly>

                                @error('yname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sname" class="col-md-3 col-form-label text-md-right">支店名</label>

                            <div class="col-md-8">
                                <input id="sname" type="text" class="form-control-plaintext fcsnon" name="sname" value="{{ $user['sname'] }}" required autocomplete="sname" autofocus readonly>

                                @error('sname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="license" class="col-md-3 col-form-label text-md-right">宅建免許番号</label>

                            <div class="col-md-8">
                                <input id="license" type="text" class="form-control-plaintext fcsnon" name="license" value="{{ $user['license'] }}" required autocomplete="license" readonly>

                                @error('license')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="zip" class="col-md-3 col-form-label text-md-right">郵便番号</label>
                            <span class="p-country-name" style="display:none;">Japan</span>

                            <div class="form-inline col-md-8">
                                <input id="zip" type="text" class="col-md-4 form-control-plaintext fcsnon" name="zip" value="{{ $user['zip'] }}" required autocomplete="zip" readonly>

                                @error('zip')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <!--input id="zip_search" type="button" class="col-md-6 form-control btn btn-primary" name="zip_search" style="display: inline-block" required autocomplete="zip_search" value="郵便番号から住所入力"-->
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address1" class="col-md-3 col-form-label text-md-right">住所１</label>

                            <div class="col-md-8">
                                <input id="address1" type="hidden" name="address1" value="{{ $user['address1'] }}" required autocomplete="address1" readonly>
                                <input id="address1" type="text" class="form-control-plaintext fcsnon" value="{{ config("area.address1.{$user['address1']}") }}" readonly>

                                @error('address1')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address2" class="col-md-3 col-form-label text-md-right">住所２</label>

                            <div class="col-md-8">
                                <input id="address2" type="text" class="form-control-plaintext fcsnon" name="address2" value="{{ $user['address2'] }}" required autocomplete="address2" readonly>

                                @error('address2')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address3" class="col-md-3 col-form-label text-md-right">住所３</label>

                            <div class="col-md-8">
                                <input id="address3" type="text" class="form-control-plaintext fcsnon" name="address3" value="{{ $user['address3'] }}" autocomplete="address3" readonly>

                                @error('address3')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tel" class="col-md-3 col-form-label text-md-right">電話番号</label>

                            <div class="col-md-8">
                                <input id="tel" type="text" class="form-control-plaintext fcsnon" name="tel" value="{{ $user['tel'] }}" required autocomplete="tel" readonly>

                                @error('tel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fax" class="col-md-3 col-form-label text-md-right">FAX番号</label>

                            <div class="col-md-8">
                                <input id="fax" type="text" class="form-control-plaintext fcsnon" name="fax" value="{{ $user['fax'] }}" required autocomplete="fax" readonly>

                                @error('fax')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        @if( $complete )

                        <div class="row">
                          <div class="col-sm-12 text-center">
                            <input type="button" name="close" class="form-control btn btn-primary" style="width:200px;" value="HOMEへ戻る" onclick="linkurl('home')"><br>
                          </div>
                        </div>

                        @else

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <input type="submit" name="send" class="btn btn-primary" style="width:180px" value="送信">
                            </div>
                        </div><br>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" name="back" class="btn btn-secondary" style="width:180px" value="true">
                                    戻る
                                </button>
                            </div>
                        </div>

                        @endif

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
