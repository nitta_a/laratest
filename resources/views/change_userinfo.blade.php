@extends('layouts.user.app')

@section('script')
    <script src="https://yubinbango.github.io/yubinbango/yubinbango.js" charset="UTF-8"></script>
    <script src="{{ asset('js/address.js') }}" type="text/javascript"></script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('会員情報の変更') }}</div>

                <div class="card-body">
                  <!--span class="">メールアドレスの変更</span><br>
                  <span class="">メールアドレスを変更する場合は、新しいメールアドレスに認証用URLをお送りしますので、アドレスのみ先に変更してください。</span>
                    <form method="POST" action="{{ route('changeusermail') }}" class="h-adr">
                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('現在のメールアドレス') }}</label>
                            <div class="col-md-8">{{ $user['email']}}
                            </div>
                            <br><br>
                            <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('新しいメールアドレス') }}</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <br><br>
                            <label for="email" class="col-md-3 col-form-label text-md-right"></label>
                            <div class="col-md-8">
                                <input id="email_confirm" type="email_confirm" class="form-control @error('email_confirm') is-invalid @enderror" name="email_confirm" value="{{ old('email_confirm') }}" required autocomplete="email_confirm" placeholder="確認のためもう一度入力してください">

                                @error('email_confirm')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </form-->
                    <form method="POST" action="{{ route('changeuserconfirm') }}" class="h-adr">
                        @csrf

                        <input type="hidden" name="id" value="{{ $id }}">
                        <div class="form-group row">
                            <label for="kname" class="col-md-3 col-form-label text-md-right">企業名</label>

                            <div class="col-md-8">
                                <input id="kname" type="text" class="form-control @error('kname') is-invalid @enderror" name="kname" value="@if(old('kname') !== null){{ old('kname') }}@elseif(isset($user['kname'])){{ $user['kname'] }} @endif" required autocomplete="kname" autofocus>

                                @error('kname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="yname" class="col-md-3 col-form-label text-md-right">屋号</label>

                            <div class="col-md-8">
                                <input id="yname" type="text" class="form-control @error('yname') is-invalid @enderror" name="yname" value="@if(old('yname') !== null){{ old('yname') }}@elseif(isset($user['yname'])){{ $user['yname'] }} @endif" autocomplete="yname" autofocus>

                                @error('yname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sname" class="col-md-3 col-form-label text-md-right">支店名</label>

                            <div class="col-md-8">
                                <input id="sname" type="text" class="form-control @error('sname') is-invalid @enderror" name="sname" value="@if(old('sname') !== null){{ old('sname') }}@elseif(isset($user['sname'])){{ $user['sname'] }} @endif" required autocomplete="sname" autofocus>

                                @error('sname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="license" class="col-md-3 col-form-label text-md-right">宅建免許番号</label>

                            <div class="col-md-8">
                                <input id="license" type="text" class="form-control @error('license') is-invalid @enderror" name="license" value="@if(old('license') !== null){{ old('license') }}@elseif(isset($user['license'])){{ $user['license'] }} @endif" required autocomplete="license">

                                @error('license')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="zip" class="col-md-3 col-form-label text-md-right">郵便番号</label>
                            <span class="p-country-name" style="display:none;">Japan</span>
                            <div class="form-inline col-md-8">
                                <input id="zip" type="text" class="p-postal-code col-md-4 form-control @error('zip') is-invalid @enderror" name="zip" value="@if(old('zip') !== null){{ old('zip') }}@elseif(isset($user['zip1']) && isset($user['zip2'])){{ $user['zip1'] }}-{{ $user['zip2'] }} @endif" required autocomplete="zip" size="8" maxlength="8">


                                <input id="zip_search" type="button" class="col-md-7 form-control btn btn-primary" name="zip_search" style="display: inline-block" required autocomplete="zip_search" value="郵便番号から住所入力" onclick="addreset()">
                                @error('zip')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address1" class="col-md-3 col-form-label text-md-right">住所１</label>

                            <div class="col-md-8">
                                <!--input id="address1" type="text" class="p-region form-control @error('address1') is-invalid @enderror" name="address1" value="@if(old('address1') !== null){{ old('address1') }}@elseif(isset($user['address1'])){{ $user['address1'] }} @endif" required autocomplete="address1"-->
                                
                                <select id="address1" name="address1" required="required" class="p-region-id form-control @error('address1') is-invalid @enderror" value="@if(old('address1') !== null){{ old('address1') }}@elseif(isset($user['address1'])){{ $user['address1'] }} @endif" required autocomplete="address1" style="max-width:180px"><option value="">都道府県を選択</option>
                                @foreach(config('area.address1') as $key => $value)
                                <option value="{{ $key }}"@if( (old('address1') !== null && old('address1') == $key) || ( isset($user['area']) && $user['area'] == $key)) selected @endif>{{ $value }}</option>
                                @endforeach
                                </select>

                                @error('address1')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address2" class="col-md-3 col-form-label text-md-right">住所２</label>

                            <div class="col-md-8">
                                <input id="address2" type="text" class="p-locality p-street-address form-control @error('address2') is-invalid @enderror" name="address2" value="@if(old('address2') !== null){{ old('address2') }}@elseif(isset($user['address2'])){{ $user['address2'] }} @endif" required autocomplete="address2" placeholder="市区町村名">

                                @error('address2')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address3" class="col-md-3 col-form-label text-md-right">住所３</label>

                            <div class="col-md-8">
                                <input id="address3" type="text" class="p-extended-address form-control @error('address3') is-invalid @enderror" name="address3" value="@if(old('address3') !== null){{ old('address3') }}@elseif(isset($user['address3'])){{ $user['address3'] }} @endif" autocomplete="address3" placeholder="番地・ビル名">

                                @error('address3')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tel" class="col-md-3 col-form-label text-md-right">電話番号</label>

                            <div class="col-md-8">
                                <input id="tel" type="text" class="form-control @error('tel') is-invalid @enderror" name="tel" value="@if(old('tel') !== null){{ old('tel') }}@elseif(isset($user['tel'])){{ $user['tel'] }} @endif" required autocomplete="tel">

                                @error('tel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fax" class="col-md-3 col-form-label text-md-right">FAX番号</label>

                            <div class="col-md-8">
                                <input id="fax" type="text" class="form-control @error('fax') is-invalid @enderror" name="fax" value="@if(old('fax') !== null){{ old('fax') }}@elseif(isset($user['fax'])){{ $user['fax'] }} @endif" required autocomplete="fax">

                                @error('fax')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary" style="width:180px">
                                    次へ
                                </button>
                            </div>
                        </div><br>
                        <!--div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="button" class="btn btn-secondary" style="width:180px">
                                    戻る
                                </button>
                            </div>
                        </div-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
