<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
	<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{ asset('favicon.ico') }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vacancy.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-theme.css') }}" rel="stylesheet">
    @yield('style')

    <!-- Scripts -->
    <!--script src="{{ asset('js/app.js') }}" defer></script-->
    <script src="{{ asset('js/fixed_midashi.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/process.js') }}" type="text/javascript"></script>

    <!-- jQuery読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
    @yield('script')
    <script type="text/javascript">
    <!--
    window.onload = function()
    {
        FixedMidashi.create();
    };

    // 入力クリア
    $(function() {
        $("input[name=clear]").bind("click", function(){
            $(this.form).find("textarea, :text, select, input[type='tel']").not(":button, :submit, :reset, :hidden").val("").end().find(":checked").prop("checked", false);
            $('input[type="checkbox"]').removeAttr('checked').prop('checked', false).change();
            $('input[name="search"]').click();
        });
    });
    // 検索フォームのchangeで検索開始
    $(function() {
        $("form#search").find(":text, select, :checkbox, input[type='tel']").not(":button, :submit, :reset, :hidden").change( function(){
            $('input[name="search"]').click();
        });
    });

    $(document).ready(function(){
        //var a_ele = $(".pls_tip");  //備考プラス（追加）
        //var b_ele = $(".cls_tip");  //備考追加クローズ
        $(".pls_tip").click(function(e){
            var div_ele = $(".lf_tooltip:hover .lf_tooltiptext");
            div_ele.css({"opacity":1, "display":"block"});
        });
        $(".cls_tip").click(function(e){
            var div_ele = $(".lf_tooltip:hover .lf_tooltiptext");
            div_ele.css({"display":"none"});
        });
    });

    // モーダルウィンドウをエンターで閉じる
    function keydown_enter() {
       if(window.event.keyCode == 13) {
         document.getElementById("modalclose").click();
       }
    }

    //-->
    </script>


</head>
<body class="bgw">
    <!-- loding -->
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
@if(Session::has('flashmessage'))
    <!-- モーダルウィンドウの中身 -->
    <div class="modal fade" id="myModal" tabindex="-1"
         role="dialog" aria-labelledby="label1" aria-hidden="true" onkeydown="keydown_enter();">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="modalclose" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    {{ session('flashmessage') }}
                </div>
                <div class="modal-footer text-center">
                </div>
            </div>
        </div>
    </div>
@endif
<!-- usermemo modal start -->
<div class="modal" id="modalForm" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
    </button>
   </div>
   <form id="usermemoUpd" method="post" action="userlist">
   @csrf
    <input type="hidden" name="mode" value="" style="display:none">
    <input type="hidden" name="userId" value="" style="display:none">
    <input type="hidden" name="memo_id" value="" style="display:none">
    <div id="result"></div>
   </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
      </div>
  </div>
 </div>
</div>
<!-- usermemo modal end   -->

    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="wide-container">
                <a class="navbar-brand" href="{{ url('/admin/home') }}">
                    {{ config('app.name', 'Laravel') }}<span class="pl-3 small">▲TOP</span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @unless (Auth::guard('admin')->check())
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('admin.login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('admin.register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin.register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('admin.logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endunless
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!--script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
        // モーダルウィンドウ
        $(window).on('load',function(){
            $('#myModal').modal('show');
        });
        // モーダルが開いた時の処理
        /*$('#modalForm').on('show.bs.modal', function (event) {
            //モーダルを開いたボタンを取得
            var button = $(event.relatedTarget);
            var id = button.name();
            //モーダル自身を取得
            var modal = $(this);
        });*/

    </script>
</body>
</html>
