@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="login">
                <div class="login-header">{{ __('内覧方法') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{  }}">
                        @csrf

                    <div class="login-form">
                      <p>ご担当者様のお名前・電話番号をご入力ください。</p>
                        <div class="form-group row">
                            <label for="uid" class="col-md-4 col-form-label text-md-right">担当者</label>

                            <div class="col-md-6">
                                <input id="name" type="name" class="form-control @error('uid') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">電話番号</label>

                            <div class="col-md-6">
                                <input id="tel" type="tel" class="form-control @error('tel') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('tel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('内覧方法表示') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
