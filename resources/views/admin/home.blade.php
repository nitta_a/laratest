@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">メニュー</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div><ul id="menulist">
                        @foreach(config('menu.admin') as $key => $value)
                        <li><a href="{{ url('/admin') }}/{{$key}}">{{ $value }}</a></li>
                        @endforeach
                    </ul></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
