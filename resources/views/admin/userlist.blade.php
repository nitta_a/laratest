@extends('layouts.admin.app')

@section('script')
<script src="{{ asset('js/userlist.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/usermemo.js') }}" type="text/javascript"></script>
@endsection

@section('content')

<?php
$today = date('Y-m-d');
?>

<div class="applist">
    <div class="row header">
    </div>
    <span class="txt">ユーザー一覧</span>
    <form id="search" method="post" action="userlist">
    @csrf
    <input type="hidden" name="mode" value="search" style="display:none">
    <div class="form-inline line col-sm-12">
        <span class="txt lmg5">登録状況</span>
        <select name="entry" class="form-control lmg5">
            <option value=""></option>
            <option value="1" @if(old('entry') == "1")selected @endif>本登録</option>
            <option value="0" @if(old('entry') == "0")selected @endif>仮登録</option>
            <option value="99" @if(old('entry') == "99")selected @endif>--↓削除済み--</option>
            <option value="8" @if(old('entry') == "8")selected @endif>否認</option>
            <option value="9" @if(old('entry') == "9")selected @endif>退会</option>
            <option value="2" @if(old('entry') == "2")selected @endif>停止</option>
        </select>
        <span class="txt lmg5">電話番号</span>
        <input type="tel" name="tel" size="12" value="{{ old('tel') }}" class="form-control lmg5">
        <span class="txt lmg5">メールアドレス</span>
        <input type="text" name="email" size="17" value="{{ old('email') }}" class="form-control lmg5">
        <input type="checkbox" name="noaccess" value="1" class="form-control lmg5" @if(old('noaccess') == "1") checked @endif >
        １年以上アクセスなし
    </div>
    <div class="form-inline line col-sm-12">
        <span class="txt lmg5">企業名 OR 屋号</span>
        <input type="text" name="kname" size="36" value="{{ old('kname') }}" class="form-control lmg5" ime-mode:active>
        <input type="submit" class="btn btn-primary rmg5" value="検索" name="search">
        <input type="button" class="btn btn-primary rmg5" value="入力クリア" name="clear">
    </div>
    </form>
    <br>
    <form method="post" name="list" id="list" action="userlist">
        @csrf
        
        <input type="hidden" name="mode" value="">
        <input type="hidden" name="userId" value="">
        <div id="div1" class="scroll_div" style="padding: 0px 1px 1px 0px;width:99%">
        <table id="table1" _fixedhead="cols:2;">
          <thead>
            <tr>
            <th class="col50">No.</th>
            <th class="col200">企業名</th>
            <th class="col100">登録状況</th>
            <th class="col130">操作</th>
            <th class="col150">屋号</th>
            <th class="col100">支店名</th>
            <th class="col150">宅建免許番号</th>
            <th class="col100">電話番号</th>
            <th class="col100">FAX番号</th>
            <th class="col300">住所</th>
            <th class="col200">メールアドレス</th>
            <th class="col150">作成日</th>
            <th class="col150">更新日</th>
            <th class="col150">最終アクセス日</th>
            </tr>
          </thead>
            @foreach($users as $user)
            <tr>
            <td class="text-center">{{ $user->id }}</td>
            <td class="kname bdr">{{ $user->kname }}</td>
            <td class="text-center col100">
            @if($user->entry == 0)<span class="kari">@endif{{ Config::get('setting.register')[$user->entry] }}@if($user->entry == 0)</span>@endif
            @if($user->entry == 0)<input type="button" name="register" value="登録" onclick="userRegister(this.name,{{ $user->id }})">@endif
            </td>
            <td class="text-center col120">
            @if($user->entry == 0)<input type="button" name="reject" value="否認" class="float-left" onclick="userRegister(this.name,{{ $user->id }})">
            @elseif($user->entry == 1)<input type="button" name="delete" value="停止" class="float-left" onclick="alertDel('{{ $user->id }}')">
            @elseif($user->entry > 1)<input type="button" name="erase" value="抹消" class="float-left" onclick="alertErase('{{ $user->id }}')">@endif
            <div class="lf_tooltip" name="lf_tooltiptext"><img src="../img/plus1.png" class="pls_tip">
            <div class="lf_tooltiptext">
            <div class="noedit">{{ $today }}</div>
            <div class="noedit">{{ Auth::user()->name }}</div><img src="../img/batsu.png" class="cls_tip"><br>
            <textarea name="usermemo[{{$user->id}}]" cols=30 rows=5 style="float:left"></textarea><br>
            <input type="button" value="追加" style="clear:both;float:right;margin-top:3px;" onclick="usermemoAdd('{{ $user->id }}')">
            </div></div>

            <span class="text">
            @if(isset($usermemo[$user->id][0]->id))
              @foreach($usermemo[$user->id] as $key => $val)
                @if($key == 0)
                <div class="cp_tooltip"><input type="button" class="bikou-ari" value="備考" onclick="usermemoUpd('{{ $user->id }}','{{ Auth::user()->name }}')">
                <div class="cp_tooltiptext">
                @endif
                {{ $val->rdate }}&nbsp;{{$val->t_user}}<br>{{ $val->memo }}<br>
              @endforeach
                </div></div>
            @else
                <input type="button" class="bikou" value="備考">
            @endif
            </span>

            <!--input type="button" class="bikou" value="備考" onclick="usermemoUpd('{{ $user->id }}','{{ Auth::user()->name }}')"-->
            </td>
            <td>{{ $user->yname }}</td>
            <td>{{ $user->sname }}</td>
            <td>{{ $user->license }}</td>
            <td>{{ $user->tel }} </td>
            <td>{{ $user->fax }} </td>
            <td class="fos">{{ $user->address1 }}{{ $user->address2 }}{{ $user->address3 }} </td>
            <td>{{ $user->email }} </td>
            <td>{{ $user->created_at }} </td>
            <td>{{ $user->updated_at }} </td>
            <td>{{ $user->login_at }} </td>
            </tr>
            @endforeach
            <tr class="yobi">
              <td> </td>
              <td></td>
              <td colspan="13"></td>
            </tr>
        </table>
        </div>
    </form>
</div>
@endsection
