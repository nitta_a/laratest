@extends('layouts.admin.app')

@section('style')
<!-- カレンダーcss読み込み -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" >
@endsection

@section('script')
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
    <script src="{{ asset('js/upfilelist.js') }}" type="text/javascript"></script>
    <!-- BootstrapのJS読み込み -->
    <script src="../js/bootstrap.min.js"></script>
    <script type="text/javascript">
    // カレンダー
    $(function() {
      $("#datepicker_1").datepicker();
      $("#datepicker_1").datepicker("option", "showOn", 'both');
      $("#datepicker_1").datepicker("option", "buttonImageOnly", true);
      $("#datepicker_1").datepicker("option", "buttonImage", '../img/calendar.png');
      $("#datepicker_1").val('{{ old("rdate") }}');
    });

    $(function() {
        // 1. 「全選択」する
        $('#all').on('click', function() {
          $("input[name='upflg[]']").prop('checked', this.checked);
        });
        // 2. 「全選択」以外のチェックボックスがクリックされたら、
        $("input[name='upflg[]']").on('click', function() {
          if ($('#uplist :checked').length == $('#uplist :input').length) {
            // 全てのチェックボックスにチェックが入っていたら、「全選択」 = checked
            $('#all').prop('checked', true);
          } else {
            // 1つでもチェックが入っていたら、「全選択」 = checked
            $('#all').prop('checked', false);
          }
        });
      });

    $(function() {
        // 1. 「削除全選択」する
        $('#delall').on('click', function() {
          $("input[name='delflg[]']").prop('checked', this.checked);
        });
        // 2. 「全選択」以外のチェックボックスがクリックされたら、
        $("input[name='delflg[]']").on('click', function() {
          if ($('#uplist :checked').length == $('#uplist :input').length) {
            // 全てのチェックボックスにチェックが入っていたら、「全選択」 = checked
            $('#delall').prop('checked', true);
          } else {
            // 1つでもチェックが入っていたら、「全選択」 = checked
            $('#delall').prop('checked', false);
          }
        });
      });

    </script>
@endsection

@section('content')



<?php
$today = date('Y-m-d');
?>

<div class="applist">
    <div class="row header">
    </div>
    <span class="txt">入居申請アップロードファイル一覧</span>
    <form id="search" method="post" action="upfilelist">
    @csrf
    <input type="hidden" name="mode" value="search" style="display:none">
    <div class="form-inline line col-sm-12">
        <span class="txt lmg5">処理状況</span>
        <select name="readflg" class="form-control lmg5">
            <option value=""></option>
            <option value="0" @if(old('readflg') == "0")selected @endif>未処理</option>
            <option value="1" @if(old('readflg') == "1")selected @endif>受付済</option>
            <option value="99" @if(old('readflg') == "99")selected @endif>削除済</option>
        </select>
        <span class="txt lmg5">登録日</span>
        <input type="text" name="rdate" size="17" value="{{ old('rdate') }}" id="datepicker_1"  class="form-control lmg5">
        <span class="txt lmg5">ファイル名</span>
        <input type="text" name="original" size="36" value="{{ old('original') }}" class="form-control lmg5" ime-mode:active>
        <span class="txt lmg5">電話番号</span>
        <input type="tel" name="tel" size="12" value="{{ old('tel') }}" class="form-control lmg5">
        <input type="submit" class="btn btn-primary rmg5" value="検索" name="search">
        <input type="button" class="btn btn-primary rmg5" value="入力クリア" name="clear">
    </div>
    </form>
    <br>
    <form method="post" name="uplist" id="uplist" action="">
        @csrf
        
        <input type="hidden" name="mode" value="">
        <input type="hidden" name="fileId" value="">
        <table style="padding: 0px 1px 1px 0px;width:99%">
            <tr>
            <th class="clear" colspan="12">
            <?php $delbtn = 0;?>
        @foreach($upfiles as $upfile)
        @if($delbtn == 0 && $upfile->removed == 0)
        <div class="float-md-right">
        <a class="btn flgchange" onclick="allfileDelete()"><div class="">✔チェック済みをすべて削除</div></a>
        <label>
            <input type="checkbox" id="delall">全て選択
        </label>
        </div>
        <?php $delbtn = 1;?>
        @endif
            @if($upfile->readflg == 0 && $upfile->removed == 0)
        <div class="float-md-left">
        <input type="checkbox" id="all">全て選択
        <a class="btn flgchange" onclick="flgChange()"><div class="">✔チェック済みをすべて「受付済」にする</div></a>
        <a class="btn flgchange" onclick="zipDownload()"><div class="">✔チェック済みを一括ダウンロードする</div></a>
        </div>
            @break
            @endif
        @endforeach
            </td>
            </tr>
        </table>
        <div id="div1" class="scroll_div" style="padding: 0px 1px 1px 0px;width:99%">
        <table id="table1" _fixedhead="cols:6;">
          <thead>
            <tr>
            <th class="col50">No.</th>
            <th class="col50">チェック</th>
            <th class="col50">状況</th>
            <th class="col100">登録日</th>
            <th class="col200">ファイル名</th>
            <th class="col200">企業名</th>
            <th class="col150">屋号</th>
            <th class="col100">支店名</th>
            <th class="col100">電話番号</th>
            <th class="col100">FAX番号</th>
            <th class="col200">メールアドレス</th>
            <th class="col50">削除</th>
            </tr>
          </thead>
            @foreach($upfiles as $upfile)
            <tr>
            <td class="text-center">{{ $upfile->id }}</td>
            <td class="text-center">@if($upfile->readflg == 0 && $upfile->removed == 0)<input type="checkbox" name="upflg[]" value="{{ $upfile->id }}">@endif</td>
            <td @if($upfile->readflg == 0 && $upfile->removed == 0) class="midoku" @endif>@if($upfile->removed != 0) {{ '削除済' }} @else {{ Config::get('setting.upfile')[$upfile->readflg]  }} @endif</td>
            <td>{{ $upfile->created_at }} </td>
            <td>@if($upfile->removed == 0)<a class="upfile" onclick="fileDownload({{ $upfile->id }})">@endif{{ $upfile->original }}@if($upfile->removed == 0)</a>@endif</td>
            <td class="kname bdr">{{ $upfile->kname }}</td>
            <td>{{ $upfile->yname }}</td>
            <td>{{ $upfile->sname }}</td>
            <td>{{ $upfile->tel }} </td>
            <td>{{ $upfile->fax }} </td>
            <td>{{ $upfile->email }} </td>
            <td class="txc">@if($upfile->removed == 0)<a class="delfile" onclick="fileDel({{ $upfile->id }})"><img src="../img/562_ex_f.png" style="width:16px;height:16px;vertical-align:top;margin-top:1px"></a>
                <input type="checkbox" name="delflg[]" value="{{ $upfile->id }}" style="margin:3px 0px 0px 10px;">@endif
            </td>
            </tr>
            @endforeach
        </table>
        </div>
    </form>
</div>
@endsection
