@extends('layouts.user.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="regist">
                <div class="card-body">
メールアドレスの変更が完了しました。<br>
<br>
旧メールアドレス：{{ $old_email }}<br>
新メールアドレス：{{ $new_email }}<br>

                </div>
          <div class="row">
            <div class="col-sm-12 text-center">
              <input type="button" name="close" class="form-control btn btn-primary" style="width:200px;" value="HOMEへ戻る" onclick="linkurl('{{ url('/home') }}')"><br><br>
            </div>
          </div>

            </div>

        </div>
    </div>
</div>
@endsection
