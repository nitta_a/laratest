@extends('layouts.user.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('メールアドレスの変更') }}</div>

                <div class="card-body">
                  <span class="">メールアドレスを変更する場合は、新しいメールアドレスに認証用URLをお送りしますので、メールに記載のURLから認証をお願いします。</span><br><br>
                    <form method="POST" action="{{ route('changeusermail') }}" class="h-adr">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('現在のメールアドレス') }}</label>
                            <div class="col-md-8 py-2">{{ $user['email']}}<input type="hidden" name="mid" value="{{ $user['usermails_mid'] }}">
                            </div>
                            <br><br>
                            <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('新しいメールアドレス') }}</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <br><br>
                            <label for="email" class="col-md-3 col-form-label text-md-right"></label>
                            <div class="col-md-8">
                                <input id="email_confirm" type="email_confirm" class="form-control @error('email_confirm') is-invalid @enderror" name="email_confirm" value="{{ old('email_confirm') }}" required autocomplete="email_confirm" placeholder="確認のためもう一度入力してください">

                                @error('email_confirm')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary w-50">
                                    {{ __('送信') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
