@extends('layouts.user.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">パスワード変更</div>

        @if (session('change_password_error'))
          <div class="container mt-2">
            <div class="alert alert-danger">
              {{session('change_password_error')}}
            </div>
          </div>
        @endif

        @if (session('change_password_success'))
          <div class="container mt-2">
            <div class="alert alert-success">
              {{session('change_password_success')}}
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 text-center">
              <input type="button" name="close" class="form-control btn btn-primary" style="width:200px;" value="HOMEへ戻る" onclick="linkurl('home')"><br><br>
            </div>
          </div>
        @else

        <div class="card-body">
          <form method="POST" action="{{route('changepassword')}}">
            @csrf
            <div class="form-group">
              <label for="current">
                現在のパスワード
              </label>
              <div>
                <input id="current" type="password" class="form-control" name="current-password" value="{{ old('current-password') }}" required autofocus>
              </div>
            </div>
            <div class="form-group">
              <label for="password">
                新しいパスワード
              </label>
              <div>
                <input id="password" type="password" class="form-control" name="new-password" value="{{ old('new-password') }}" required>
                @if ($errors->has('new-password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('new-password') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label for="confirm">
                新しいパスワード（確認用）
              </label>
              <div>
                <input id="confirm" type="password" class="form-control" name="new-password_confirmation" value="{{ old('new-password_confirmation') }}" required>
              </div>
            </div>
            <div>
              <button type="submit" class="btn btn-primary w-25">変更</button>
            </div>
          </form>
        </div>
        @endif

      </div>
    </div>
  </div>
</div>
@endsection
