@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('退会手続き') }}</div>
        @if (session('flashmessage'))

                <div class="card-body">
                        <div class="form-group row">
                            <label for="caution" class="col-md-12 col-form-label text-md-center">{{ session('flashmessage') }}</label>
                            <br>
                        </div>

                </div>
          <div class="row">
            <div class="col-sm-12 text-center">
              <input type="button" name="close" class="form-control btn btn-primary" style="width:200px;" value="HOMEへ" onclick="linkurl('{{ url('/') }}')"><br><br>
            </div>
          </div>
        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
