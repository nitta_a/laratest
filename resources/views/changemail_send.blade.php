@extends('layouts.user.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="regist">
                <div class="card-body">
ご入力いただいたメールアドレスにアドレス変更用のURLをお送りしました。<br>
URLの有効期限内（{{config('auth.verification.expire', 60)}}分以内）に認証を行い、メールアドレスの変更をお願いいたします。<br><br>

しばらく経ってもメールが届かない場合<br>
・迷惑メールに振り分けされていないかどうかご確認ください。<br>
・ご登録メールアドレスに誤りがないかご確認のうえ、再度お試しください。
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
