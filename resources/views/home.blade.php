@extends('layouts.user.app')

@section('script')
    <script src="{{ asset('js/userupfilelist.js') }}" type="text/javascript"></script>

    <!-- Dropzone -->
    <script src="{{ asset('dropzone/dropzone.js') }}"></script>
    <!--script src="{{ asset('dropzone/upload.js') }}"></script-->
    <script type="text/javascript">
    //ファイルアップロード
    $(function(){
		var _sumfilesize = {{(int)$filesize}};
		const _maxfilesize = {{(int)config('setting.maxupfilesize')}};
		const _overmessage = "アップロード可能な合計ファイルサイズを超えました。";
        $('#image_drop_area').dropzone({
            url:'{{ url("/fileUpload") }}',
            paramName:'file',
            params: {'_token':'{{ csrf_token() }}'},
            maxFilesize:{{number_format(config('setting.upfilesize') / 1048576, 0)}}, //MB 
            addRemoveLinks:true,
            previewsContainer:'#preview_area',
            thumbnailWidth:100, //px
            thumbnailHeight:100, //px
            uploadprogress:function(_file, _progress, _size){
                _file.previewElement.querySelector("[data-dz-uploadprogress]").style.width = "" + _progress + "%";
            },
            success:function(_file, _return, _xml){
				if (_return.result === false) {
					_file.previewElement.classList.add("dz-error");
					_file.previewElement.querySelector("[data-dz-errormessage]").textContent = _return.message;
				} else if(_maxfilesize < (_sumfilesize + parseInt(_return.filesize))) {
					_file.previewElement.classList.add("dz-error");
					_file.previewElement.querySelector("[data-dz-errormessage]").textContent = _overmessage;
				} else {
					_sumfilesize += parseInt(_return.filesize);
					_file.previewElement.classList.add("dz-success");
					_file.previewElement.querySelector("[data-dz-name]").dataset.dzName = _return.hashname;
					_file.previewElement.querySelector("[data-dz-size]").dataset.dzSize = _return.filesize;
				}
            },
			complete:function(){},
            error:function(_file, _error_msg){
                //var ref;
                //(ref = _file.previewElement) != null ? ref.parentNode.removeChild(_file.previewElement) : void 0;
				const ref = _file.previewElement;
				if (ref !== null) {
					ref.classList.add("dz-error");
					if (_error_msg.message === undefined) {
						ref.querySelector("[data-dz-errormessage]").textContent = _error_msg;
					} else {
						if (_error_msg.message === "CSRF token mismatch.") {
							ref.querySelector("[data-dz-errormessage]").textContent = "認証エラーです。再ログインしてください。";
						} else {
							ref.querySelector("[data-dz-errormessage]").textContent = "エラーが発生しました。";
						}
					}
				}
				if (_file._removeLink) {
					_file._removeLink.textContent = this.options.dictCancelUpload;
				}
            },
            removedfile:function(_file){
                //var ref;
                //(ref = _file.previewElement) != null ? ref.parentNode.removeChild(_file.previewElement) : void 0;
				const ref = _file.previewElement;
				if (ref !== null) {
					if ($(ref).hasClass("dz-success")) {
						_sumfilesize -= parseInt(ref.querySelector("[data-dz-size]").dataset.dzSize);
					}
					ref.parentNode.removeChild(_file.previewElement)
				}
            },
            previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-details\">\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n    <div class=\"dz-size\" data-dz-size></div>\n    <img data-dz-thumbnail />\n  </div>\n  <div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress></span></div>\n  <div class=\"dz-success-mark\"><span>✔</span></div>\n  <div class=\"dz-error-mark\"><span>✘</span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>",
            dictRemoveFile:'削除',
            dictCancelUpload:'キャンセル',
			dictFileTooBig:'アップロード可能なファイルサイズを超えています。'
        });
    });

    $(document).ready(function () {
      $("body").tooltip({   
        selector: "[data-toggle='tooltip']",
        container: "body"
      })
        //Popover, activated by clicking
        .popover({
        selector: "[data-toggle='popover']",
        container: "body",
        html: true
      });
    });

    $(function() {
        // 1. 「全選択」する
        $('#all').on('click', function() {
          $("input[name='delflg[]']").prop('checked', this.checked);
        });
        // 2. 「全選択」以外のチェックボックスがクリックされたら、
        $("input[name='delflg[]']").on('click', function() {
          if ($('#uplist :checked').length == $('#uplist :input').length) {
            // 全てのチェックボックスにチェックが入っていたら、「全選択」 = checked
            $('#all').prop('checked', true);
          } else {
            // 1つでもチェックが入っていたら、「全選択」 = checked
            $('#all').prop('checked', false);
          }
        });
      });

    </script>

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
			@if (session('status'))
			<div class="card-body">
				<div class="alert alert-success" role="alert">
					{{ session('status') }}
				</div>
			</div>
			@endif
			<section class="home-section">
				<form method="post" id="usersub" action="">
					<div class="ttl">登録情報の変更</div>
					<div class="form-inline justify-content-between">
						<a href="changeusermail" class="searchbtn col-lg-3">メールアドレスの変更</a><!--span class="mx-3">パスワード、メールアドレス、会員情報等の変更はこちら</span-->
						<a href="changepassword" class="searchbtn col-lg-3">パスワードの変更</a>
						<a href="changeuserinfo" class="searchbtn col-lg-3">会員情報の変更</a>
						<a href="withdrawal" class="searchbtn col-lg-2">退会</a>
					</div>
				</form>
			</section>
			<section class="home-section">
				<div class="ttl">物件資料の確認・申込書</div>
				<div class="form-inline">
					<div class="col-md-7">
						<!--div class="ttl">空室速報</div-->
						<div class="form-inline">
							<a href="vacancy"  class="btn-cc btn-c colmax py-3">空室一覧・物件資料<span>物件の資料はこちら</span></a>
						</div><br>
					</div>
					<div class="col-md-5">
						申込書ダウンロード<br>
						<a href="./pdf/nyuukyo1.pdf" class="btn pdfdwn" target="_blank">
							<div class=""><img src="./img/pdf32.png">入居申込書【個人契約用】</div>
						</a>
						<a href="./pdf/nyuukyo2.pdf" class="btn pdfdwn" target="_blank">
							<div class=""><img src="./img/pdf32.png">入居申込書【法人契約用】</div>
						</a>
					</div>
				</div>
			</section>
			<section class="home-section">
				<div class="ttl">
					申込書・必要書類のアップロード
					<div class="tooltip1">
						<span class="tooltip1text">
							PDF・画像形式のファイルに対応しています。<br>
							アップロード可能なファイルサイズは最大{{ number_format(config('setting.upfilesize') / 1048576, 0) . 'MB' }}まで<br>
							合計ファイルサイズ（登録済み含む）は{{ number_format(config('setting.maxupfilesize') / 1048576, 0) . 'MB' }}までです。
						</span>
						<img src="./img/hatena.png">
					</div>
				</div>
				<div id="preview_area" class="dropzone-custom">
					<div id="image_drop_area">
						<span>ここにファイルをドラッグ＆ドロップしてください。</span>
					</div>
				</div>
				<form method="post" action="fileSave" id="fileUpload">
					{{ csrf_field() }}
					 <!-- <input type="hidden" name="filename" value=""> -->
					 <button type="button" name="upload" id="upload" class="btn btn-primary mt-1 float-right" onclick="upfile()">アップロード</button>
				</form>
				
				@if(isset($upfiles) && sizeof($upfiles) > 0 )
				<form method="post" id="uplist" action="">
					@csrf
					<input type="hidden" name="mode" value="">
					<input type="hidden" name="fileId" value="">
					<table id="tableup">
						<tr>
							<th colspan="5" style="text-align:right">
								<a class="btn flgchange" onclick="allfileDelete()"><div class="">✔チェック済みをすべて削除</div></a>
								<label>
									<input type="checkbox" id="all">全て選択
								</label>
							</th>
						</tr>
						<tr>
							<th width="17%">登録日</th>
							<th width="8%">受付</th>
							<th width="50%">登録済みファイル</th>
							<th width="15%" class="file-size">
								容量
								<span @if (config('setting.maxupfilesize') * 0.9 < $filesize) class="near-max" @endif>
									{{ formatSizeUnits($filesize) }} / {{ number_format(config('setting.maxupfilesize') / 1048576, 0) . 'MB' }}
								</span>
							</th>
							<th width="10%">削除</th>
						</tr>
						<?php $uketuke = array(0 => '<span class="midoku2">未</span>', 1 => '済'); ?>
						@foreach($upfiles as $upfile)
						<tr>
							<td class="txc">{{ $upfile->created_at }}</td>
							<td class="txc">{!! $uketuke[$upfile->readflg] !!}</td>
							<td><a class="upfile" onclick="fileDownload({{ $upfile->id }})">{{ $upfile->original }}</a></td>
							<td class="txr file-size">{{ formatSizeUnits($upfile->size) }}</td>
							<td class="txc"><a class="delfile" onclick="fileDel({{ $upfile->id }})"><img src="img/562_ex_f.png" style="width:16px;height:16px;vertical-align:top;margin-top:1px"></a>
								<input type="checkbox" name="delflg[]" value="{{ $upfile->id }}" style="margin:3px 0px 0px 10px;">
							</td>
						</tr>
						@endforeach
					</table>
				</form>
				@endif
			</section>
        </div>
    </div>
</div>
@endsection
<?php
    // ファイルサイズ表示変換
    function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
?>