<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('/welcome', function () {
    return view('welcome');
});
*/
//Auth::routes();
Auth::routes(['verify' => true]);

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->middleware('verified')->name('home');
Route::get('/home', 'HomeController@index')->middleware('verified')->name('home');

/*
Route::get('/verified', function(){
    return view('auth.verified');
})->middleware('verified');
*/
/*
Route::get('/email/verify/{id}/{hash}', [
    'as' => 'verify',
    'uses' => 'Auth\VerificationController@emailVerifyComplete',
])->name('verification.verify');
*/

//Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@emailVerifyComplete')->name('verification.verify');
Route::get('email/verify/{id}/{hash}', 'Auth\EmailVerificationController@verify')->name('verification.verify');
Route::post('/post_register', 'Auth\PostRegisterController@register')->name('post_register');
Route::post('/post_confirm', 'Auth\PostRegisterController@confirm')->name('post_confirm');
Route::get('/register_send', 'Auth\RegisterController@afterview')->name('register_send');
Route::get('/post_send', 'Auth\PostRegisterController@afterview')->name('post_send');
Route::post('/post_send', 'Auth\PostRegisterController@afterview')->name('post_send');
Route::get('/changepassword', 'HomeController@showChangePasswordForm');
Route::post('changepassword', 'HomeController@changePassword')->name('changepassword');
Route::get('/changeuserinfo', 'HomeController@showChangeUserinfoForm');
Route::post('changeuserinfo', 'HomeController@changeUserinfo')->name('changeuserinfo');
Route::post('changeuserconfirm', 'HomeController@confirm')->name('changeuserconfirm');
Route::post('changeuserregist', 'HomeController@regist')->name('changeuserregist');
Route::get('changeusermail', 'HomeController@showChangeUsermailForm')->name('changeusermail');
Route::post('changeusermail', 'EmailRegisterController@register')->name('changeusermail');
Route::get('emailchange/verify/{id}/{hash}', 'EmailVerificationController@verify')->name('emailchange.verify');
Route::get('changemail_send', 'EmailRegisterController@afterview')->name('changemail_send');
Route::get('vacancy', 'HomeController@showVacancy')->name('vacancy');
Route::get('vacancy/sort/{order}', 'HomeController@showVacancy')->where('order', 'bname|address')->name('vacancy_sort');
Route::get('roomdoc/{rseq}', 'HomeController@getRoomDoc')->where('req', '[0-9]+')->name('roomdoc');
//Route::get('vacancy_frame', 'HomeController@showVacancyframe')->name('vacancy_frame');
Route::get('nairan', 'AjaxController@regist')->name('nairan');
Route::get('/terms', 'Auth\PostRegisterController@termsview')->name('terms');
Route::post('fileUpload', 'AjaxController@fileUpload')->name('fileUpload');
Route::post('fileSave', 'HomeController@fileSave')->name('fileSave');
Route::post('download', 'HomeController@download')->name('download');
Route::post('fileDelete', 'HomeController@fileDelete')->name('fileDelete');
Route::get('withdrawal', 'HomeController@showWithdrawalForm')->name('withdrawal');
Route::post('withdraw', 'HomeController@withDraw')->name('withdraw');
Route::match(array('GET', 'POST'),'allfiledel', 'HomeController@allfileDelete')->name('allfiledel');

//Route::get('changemail_close', 'EmailVerificationController@afterview')->name('changemail_close');

//Route::post('password/reset', 'Auth\MyResetPasswordController@reset')->name('password.update');

// 後で消す

//Route::get('user/{id}', function ($id) {
//    return 'User '.App\User::find($id);
//});

// 管理者
Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function () {

    // ログイン認証関連
    Auth::routes([
        'register' => false,
        'confirm'  => false,
        'reset'    => false
    ]);
    //Route::get('/admin', 'Admin\Auth\LoginController@showLoginForm');
    Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
    // ログイン認証後
    Route::middleware('auth:admin')->group(function () {

        // TOPページ
        //Route::resource('home', 'HomeController', ['only' => 'index']);
        Route::match(array('GET', 'POST'),'home', 'HomeController@index')->name('home');
        //Route::resource('/', 'HomeController', ['only' => 'index']);
        Route::match(array('GET', 'POST'),'/', 'HomeController@index')->name('home');
        // ユーザー一覧ページ
        //Route::resource('userlist', 'UserlistController@index');
        Route::match(array('GET', 'POST'),'userlist', 'UserlistController@index')->name('userlist');
        Route::match(array('GET', 'POST'),'upfilelist', 'UpfilelistController@index')->name('upfilelist');
        Route::get('useradd', 'UserlistController@update')->name('useradd');
        //Route::match(array('GET', 'POST'), 'getUsermemoList', 'AjaxController@getUsermemoList');
        Route::post('getUsermemoList', 'AjaxController@getUsermemoList')->name('getUsermemoList');
        Route::post('download', 'UpfilelistController@download')->name('download');
        Route::post('fileDelete', 'UpfilelistController@fileDelete')->name('fileDelete');
        Route::match(array('GET', 'POST'),'allfiledel', 'UpfilelistController@allfileDelete')->name('allfiledel');

    });

});


