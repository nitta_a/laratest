<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\User;
use App\Userupfile;
use Auth;
use Hash;
use Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $user;
    protected $userupfile;

    public function __construct(User $user, Userupfile $userupfile)
    {
        $this->middleware('auth');
        //$this->middleware(['auth','verified']);
        $this->user = $user;
        $this->userupfile = $userupfile;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $first_login = \Session::get('first_login', 0 );
        if($first_login){
            return view('change_password');
        }else{
            $upfiles = $this->upfileList();
			$filesize = $this->userupfile
					->selectRaw('IFNULL(SUM(size), 0) AS sumsize')
					->where('users_id', Auth::user()->id)
					->where('removed', 0)
					->first()
					->sumsize;
            return view('home', compact('upfiles', 'filesize'));
        }
    }
    // パスワード変更画面表示
    public function showChangePasswordForm()
    {
        return view('change_password');
    }
    // 空室速報表示
    public function showVacancy(string $sort = null)
    {
		if ($sort === null) {
			$sort = "address";
		}
		$kyoju = $this->getVacancy('010', $sort);
		$zigyo = $this->getVacancy('020', $sort);
		
		$lastsynctime = DB::table('vacancy')->max('synctime');
		$synctime = date("Y/m/d H:00:00");
		if (strtotime($synctime) < strtotime($lastsynctime)) {
			$synctime = $lastsynctime;
		}
		
		return view('vacancy', compact('kyoju', 'zigyo', 'sort', 'synctime'));
    }
	// 空室速報用データ取得
	private function getVacancy (string $code, string $sort) {
		$table = DB::table('vacancy')->where('shubetu', $code);
		
		switch ($sort) {
			case "address":
				$table->orderBy('shubetu');
				$table->orderByRaw('bsort IS NULL ASC')->orderBy('bsort');
				$table->orderBy('bseq');
				$table->orderByRaw('rsort IS NULL ASC')->orderBy('rsort');
				$table->orderBy('rseq');
				break;
			case "bname":
				$table->orderBy('shubetu');
				$table->orderBy('bname');
				$table->orderBy('bseq');
				$table->orderByRaw('rsort IS NULL ASC')->orderBy('rsort');
				$table->orderBy('rseq');
				break;
		}
		return $table->get();
	}
	// 部屋資料（PDF）表示
	public function getRoomDoc (int $rseq) {
		$r = DB::table('vacancy')->where('rseq', $rseq)->first();
		
		// URL手入力対応
		// （空室速報にデータが載っていない、資料パス登録が無い、資料がない場合）
		if ($r === null || (string)$r->rpath === "" || !file_exists($r->rpath)) {
			return abort(404);
		}
		
		$path = $r->rpath;
		$ext = pathinfo($path, PATHINFO_EXTENSION);
		$type = mime_content_type($path);
		$size = filesize($path);
		$name = "物件資料_".date("md_His").".".$ext;
		
		header('Content-Type: '.$type);
		header('Content-disposition: inline; filename*=UTF-8\'\''.rawurlencode($name));
		header('Content-Length: '.$size);
		ob_clean();
		flush();
		readfile($path);
		return;
	}

    public function changePassword(Request $request) {
        //現在のパスワードが正しいかを調べる
        if(!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            return redirect()->back()->with('change_password_error', '現在のパスワードが間違っています。');
        }

        //現在のパスワードと新しいパスワードが違っているかを調べる
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            return redirect()->back()->with('change_password_error', '新しいパスワードが現在のパスワードと同じです。違うパスワードを設定してください。');
        }

        //パスワードのバリデーション。新しいパスワードは8文字以上、new-password_confirmationフィールドの値と一致しているかどうか。
        $validated_data = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:8|confirmed',
        ],[
            'new-password' => '新しいパスワード',
        ]);

        //パスワードを変更
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        \Session::forget('first_login');
        return redirect()->back()->with('change_password_success', 'パスワードを変更しました。');
    }

    public function showChangeUserinfoForm()
    {
        $id = Auth::user()->id;
        $user = $this->user->find($id);
        return view('change_userinfo', compact('id','user'));
    }
    public function showChangeUsermailForm()
    {
        $id = Auth::user()->id;
        $user = $this->user->find($id);
        return view('change_usermail', compact('id','user'));
    }
    public function showChangemailsendForm()
    {
        $id = Auth::user()->id;
        $user = $this->user->find($id);
        return view('changemail_send', compact('id','user'));
    }


    // 入力内容の確認画面表示用
    public function confirm(Request $request)
    {
        $id = Auth::user()->id;

        $this->validate($request, [
            'kname' => 'required|string|max:255',
            'sname'  => 'required|string|max:255',
            'license'  => 'required|string|max:100',
            'zip'  => 'required|string|max:8|min:7',
            'address1'  => 'required|string|max:80',
            'address2'  => 'required|string|max:80',
            'tel' => 'required|string|max:20',
            'fax' => 'required|string|max:20',
        ],[
            'kname.required' => ':attributeは必須です',
            'sname.required'  => ':attributeは必須です',
            'license.required'  => ':attributeは必須です',
            'zip.required' => ':attributeは必須です',
            'address1.required' => ':attributeは必須です',
            'address2.required'  => ':attributeは必須です',
            'tel.required'  => ':attributeは必須です',
            'fax.required'  => ':attributeは必須です',
            'kname.max' => ':attributeは最大255文字までです',
            'sname.max'  => ':attributeは最大255文字までです',
            'license.max'  => ':attributeは最大100文字までです',
            'zip.max' => ':attributeは最大8文字までです',
            'zip.min' => ':attributeは7文字です',
            'address1.max' => ':attributeは最大80文字までです',
            'address2.max'  => ':attributeは最大80文字までです',
            'tel.max'  => ':attributeは最大20文字までです',
            'fax.max'  => ':attributeは最大20文字までです',
        ],[
            'kname' => '企業名',
            'sname' => '支店名',
            'license' => '宅建免許番号',
            'zip' => '郵便番号',
            'address1' => '住所１',
            'address2' => '住所２',
            'tel'  => '電話番号',
            'fax' => 'FAX番号',
        ]);

        $user = [
            'kname' => $request->kname,
            'yname'  => $request->yname,
            'sname'  => $request->sname,
            'license'  => $request->license,
            'zip'  => $request->zip,
            'address1'  => $request->address1,
            'address2'  => $request->address2,
            'address3'  => $request->address3,
            'tel' => $request->tel,
            'fax' => $request->fax,
        ];
        return view('change_userconfirm', compact('id','user'));
    }

    public function regist(Request $request)
    {
        if (Auth::check()) {
            if ($request->get('back')) {
                // 戻るボタンが押された場合
                $input = $request->except('back');
                return redirect('changeuserinfo')->withInput($input);
            }else if($request->get('send')) {

                // 登録処理
                $result = $this->update($request);
                if(!$result){
                    \Session::flash('flashmessage','登録が失敗しました。');
                    Log::debug('登録が失敗しました。');
                }else{
                    $complete = 1;
                }
                $id = $request->id;
                $user = $this->user->find($id);
                $user['zip'] = $user['zip1']."-".$user['zip2'];
                $user['address1'] = $user['area'];
                return view('change_userconfirm', compact('id','user','complete'));

            }
        }else{
            // タイムアウトの処理
            \Session::flash('flashmessage', 'タイムアウトしました。');
            return redirect()->guest('home');
        }

    }

    public function update($request)
    {
        $user = [
            'kname' => $request->kname,
            'yname'  => $request->yname,
            'sname'  => $request->sname,
            'license'  => $request->license,
            'zip1' => substr($request->zip,0,3),
            'zip2' => substr($request->zip,-4,4),
            'area' => $request->address1,
            'address1' => config("area.address1.{$request->address1}"),
            'address2'  => $request->address2,
            'address3'  => $request->address3,
            'tel' => $request->tel,
            'fax' => $request->fax,
        ];
        $result = $this->user->where('id',$request->id )
                ->update($user);
        return $result;
    }

    /**
     * cURLを利用してjsonデータをPOST送信する
     * @param string $url   送信先URL
     * @param array $data   jsonデータ
     * @return URLページ出力のデータ(取得失敗時false)
     * ※送信先側でのデータ取得方法 $json = file_get_contents("php://input");
     */
    public function postFromHTTP($url, $data){
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_AUTOREFERER => true,
            CURLOPT_FAILONERROR => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1_2,
            CURLOPT_HTTPHEADER => ["Content-Type: application/json"],
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPAUTH  => CURLAUTH_ANY,
            CURLOPT_USERPWD  => "",
            CURLOPT_POSTFIELDS => $data
        ]);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    // 申込書アップロード（保存）
    public function fileSave(Request $request)
    {
        // 全アップロードファイルの容量制限チェック
		$maxupfilesize = config('setting.maxupfilesize');
		$filesize = $this->userupfile
				->selectRaw('IFNULL(SUM(size), 0) AS sumsize')
				->where('users_id', Auth::user()->id)
				->where('removed', 0)
				->first()
				->sumsize;
		
//        $userfiles = $this->userupfile
//            ->where('users_id',Auth::user()->id )
//            ->where('removed', 0 )
//            ->get();
//        $filesize = 0;
//        foreach($userfiles as $file){
//            $filesize += $file->size;
//        }
        $today = date('Y-m-d');
        $file = $request->filename;
//        $files = array();
//        $files = explode(',',$file);
		$filenames = $request->filenames;
		$hashnames = $request->hashnames;
        $uid = Auth::user()->id;
		
        foreach((array)$filenames as $key => $value){
            DB::beginTransaction();
            try {
                Log::debug("filename=".$value);
				$num = $this->userupfile
						->selectRaw('IFNULL(MAX(num) + 1, 1) AS num')
						->where( 'users_id', $uid )
						->first()
						->num;
                //$tmpfile = 'tmp/'.$uid."/".$value;
				$tmpfile = $hashnames[$key];
                $size = Storage::size($tmpfile);
                $filesize += $size;
                if ($maxupfilesize < $filesize) {
                    \Session::flash('flashmessage','アップロード可能なサイズを超えました。');
                    DB::rollback();
                    return redirect('home');
                }
				
				$basename = pathinfo($tmpfile, PATHINFO_BASENAME);
				$alias = "public/{$uid}/{$today}/{$basename}";
				
				Storage::makeDirectory('public/'.$uid);
				Storage::makeDirectory('public/'.$uid."/".$today);
				Storage::move($tmpfile, $alias);
				
                $upfile = [
                    'users_id' => $uid,
                    'num' => $num,
                    'rdate' => Carbon::now(),
                    't_uid'  => $uid,
                    'original' => $value,
                    'alias' => $alias,
                    'size' => $size,
                    'created_at' => Carbon::now(),
                ];
                $id = $this->userupfile->insertGetId($upfile);
                if(!$id){
                    \Session::flash('flashmessage','登録が失敗しました。');
                    Log::debug('登録が失敗しました。');
                }else{
                    //ユーザーディレクトリ作成
                    //Storage::makeDirectory('public/'.$uid);

                    //日付フォルダ作成
                    //Storage::makeDirectory('public/'.$uid."/".$today);

                    //tmpフォルダからユーザーフォルダへ移動
                    //Storage::move('tmp/'.$uid."/".$value, 'public/'.$uid."/".$today."/".$value);
                }
            } catch (\Exception $e) {
                Log::debug($e);
                DB::rollback();
                \Session::flash('flashmessage','登録が失敗しました。');
                return redirect('home');
            }
            DB::commit();
            \Session::flash('flashmessage','登録が完了しました。');
        }
        //tmpフォルダの過去のファイル削除
        Storage::delete(Storage::allFiles('tmp/'.$uid."/"));

        return redirect('home');
        
    }
    // 登録済み申込書一覧を取得
    public function upfileList()
    {

        $upfiles = $this->userupfile
                       ->where( 'users_id',Auth::user()->id )
                       ->where( 'removed', 0 )
                       ->orderBy( 'num','desc' )
                       ->get();
        return $upfiles;

    }
    public function getFile($id)
    {
        $file = $this->userupfile
                       ->where( 'userupfile.id', $id )
                       ->where( 'userupfile.removed', 0 )
                       ->first();
        return $file;
    }

    // ファイルのダウンロード
    public function download(Request $request)
    {
        $fileId = $request->fileId;
        $file = $this->getFile($fileId);
        //$filePath = "public/".$file->users_id."/".$file->rdate."/".$file->alias;
		$filePath = $file->alias;

        //$fileName = $file->alias;
		$fileName = $file->original;

        $mimeType = Storage::mimeType($filePath);

        $headers = [['Content-Type' => $mimeType]];

        return Storage::download($filePath, $fileName, $headers);
    }
    // ファイルの削除
    public function fileDelete(Request $request)
    {
        $uid = Auth::user()->id;
        $uname = Auth::user()->kname;
        $fileId = $request->fileId;
        $file = $this->getFile($fileId);
        //$filePath = "public/".$file->users_id."/".$file->rdate."/".$file->alias;
		$filePath = $file->alias;

        //$fileName = $file->alias;
        DB::beginTransaction();
        try {
            $result = DB::table('userupfile')->where('id',$fileId )
                ->update(
                [
                    'up_uid' => $uid,
                    'up_user' => $uname,
                    'deldate' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'removed' => $fileId,
                ]
            );
            if(!$result){
                DB::rollback();
                // 接続エラー処理
                \Session::flash('flashmessage','ファイル削除が失敗しました。');
                Log::debug('ファイル削除が失敗しました。');
                return;
            }
            //ファイル削除
            Storage::delete($filePath);
        } catch (\Exception $e) {
            Log::debug($e);
            DB::rollback();
            \Session::flash('flashmessage','ファイル削除が失敗しました。');
            Log::debug('ファイル削除が失敗しました。');
            return;
        }
        DB::commit();
        return redirect('home');

    }

    // ファイルの一括削除
    public function allfileDelete(Request $request)
    {
        $uid = Auth::user()->id;
        $uname = Auth::user()->kname;
        $delflg = $request->delflg;
        foreach((array)$delflg as $value){
            $file = $this->getFile($value);
            //$filePath = "public/".$file->users_id."/".$file->rdate."/".$file->alias;
			$filePath = $file->alias;

            //$fileName = $file->alias;
            DB::beginTransaction();
            try {
                $result = DB::table('userupfile')->where('id',$value )
                    ->update(
                    [
                        'up_uid' => $uid,
                        'up_user' => $uname,
                        'deldate' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'removed' => $value,
                    ]
                );
                if(!$result){
                    DB::rollback();
                    // 接続エラー処理
                    \Session::flash('flashmessage','ファイル削除が失敗しました。');
                    Log::debug('ファイル削除が失敗しました。');
                    return;
                }
                //ファイル削除
                Storage::delete($filePath);
            } catch (\Exception $e) {
                Log::debug($e);
                DB::rollback();
                \Session::flash('flashmessage','ファイル削除が失敗しました。');
                Log::debug('ファイル削除が失敗しました。');
                return;
            }
            DB::commit();
        }

        return redirect('home');
    }



    // 退会画面表示
    public function showWithdrawalForm()
    {
        return view('withdrawal');
    }
    // 退会
    public function withDraw(Request $request) {
        //注意事項同意
        if(!isset($request->consent)){
            return redirect()->back()->with('consent_error', '注意事項に同意してください。');
        }
        //パスワードが正しいかを調べる
        if(!(Hash::check($request->get('password'), Auth::user()->password))) {
            return redirect()->back()->with('password_error', 'パスワードが間違っています。');
        }
        $id = Auth::user()->id;
        $uid = Auth::user()->uid;
        DB::beginTransaction();
        try {
            $result = DB::table('users')->where('id',$id )
                ->update(
                [
                    'entry' => 9,
                    'update_user' => $uid,
                    'deldate' => Carbon::now(),
                    'updated_at' => NOW(),
                    'removed' => $id,
                ]
            );
            if(!$result){
                DB::rollback();
                // 接続エラー処理
                \Session::flash('flashmessage','ユーザー退会手続きが失敗しました。');
                Log::debug('ユーザー退会手続きが失敗しました。');
                return;
            }
            // ユーザーメールアドレス
            $usermails_mid = $this->user->where('id', $id)->first()->usermails_mid;

            $result = DB::table('usermails')->where('mid',$usermails_mid )
                ->update(
                [
                  'removed' => $usermails_mid,
                  'update_user' => $uid,
                  'updated_at' => Carbon::now(),
                  'deldate' => Carbon::now(),
                ]
            );
            if(!$result){
                DB::rollback();
                // 接続エラー処理
                \Session::flash('flashmessage','ユーザー退会手続きが失敗しました。');
                Log::debug('ユーザー退会手続きが失敗しました。');
                return redirect('home');
            }
            // アップロードファイル登録テーブル削除
            $count = DB::table('userupfile')->selectRaw('count(*) as cnt')->where('users_id',$id)->first()->cnt;
            if($count > 0){
                $result = DB::statement("
                    UPDATE userupfile SET 
                        `removed` = `id`,
                        `up_uid` = ?,
                        `up_user` = '?',
                        `updated_at` = NOW(),
                        `deldate` = CURDATE()
                    WHERE `users_id` = ?" ,[ $id, $uid, $id ] );
                Log::debug('userupfile:'.$result);

                if(!$result){
                    // 接続エラー処理
                    Log::debug("userupfile:delete失敗");
                    DB::rollback();
                    return redirect('home');
                }else{
                    // ファイル保存ディレクトリ削除
                    Storage::deleteDirectory('tmp/'.$id);
                    Storage::deleteDirectory('public/'.$id);
                    Log::debug('ディレクトリ削除');
                }
            }
        } catch (\Exception $e) {
            Log::debug($e);
            DB::rollback();
            \Session::flash('flashmessage','ユーザー退会手続きが失敗しました。');
            Log::debug('ユーザー退会手続きが失敗しました。');
            return redirect('home');
        }
        
        DB::commit();
        Auth::logout();
        \Session::flash('flashmessage','ユーザー退会手続きが完了しました。');
        return view('finish');
    }

}
