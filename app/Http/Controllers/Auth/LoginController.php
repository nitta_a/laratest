<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

use Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function username()
    {
        return 'uid';
    }
    /* ログイン条件の変更 */
    public function credentials(Request $request)
    {
        $arr = $request->only($this->username(), 'password');
        $arr['users.removed'] = 0;    // 削除フラグの条件を追加

        return $arr;
    }

    // 初回ログイン時にパスワード変更画面へ遷移する
    protected function authenticated(\Illuminate\Http\Request $request, $user)
    {
        // アクセスログを記録する
        $res = DB::table('login_log')->insert(
                  [
                   'users_id' => Auth::user()->id,
                   'ipaddr' => $request->ip(),
                   'host' => $request->getHttpHost(),
                   'useragent' => $request->userAgent(),
                   'login_at' => Carbon::now(),
                   'created_user' => Auth::user()->kname,
                   'created_at' => Carbon::now()
                  ]
            );
        if(!$res){
            Log::debug('login_logテーブルへの挿入に失敗しました');
        }
        // 最終アクセス日時を更新する
        $res = DB::table('users')->where('id',Auth::user()->id)
            ->update([
              'login_at' => Carbon::now(),
        ]);
        if(!$res){
            Log::debug('usersテーブルの更新に失敗しました');
        }
        Log::debug("Auth::user->id=".Auth::user()->id);
        $val = \Cookie::get('COOKIE_NAME');
        $cookie = \Cookie::get('key');
        Log::debug('Cookie:'.$cookie);
        
        if($request->uid == $request->password){
            //$this->redirectTo = '/changepassword';
            \Session::put('first_login','1');
            return redirect('/changepassword');
        }
    }


}
