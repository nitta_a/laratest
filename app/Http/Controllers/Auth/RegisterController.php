<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Usermail;
use App\Rules\EncryptEmail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Validation\Rule;
use Illuminate\Support\Carbon;
use Log;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    protected $request;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/register_send';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guest');
        $this->request = $request;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // emailと確認用の入力チェック（順番考慮）
        $this->validate($this->request, [
            'email' => 'required|email|max:255',
            'email_confirm' => 'required|email|max:255|same:email',
        ],[
            'email.required' => ':attributeは必須です',
            'email.email' => ':attribute形式が正しくありません',
            'email.max255'  => ':attributeは255字以内です',
            'email_confirm.required' => ':attributeは必須です',
            'email_confirm.email' => ':attribute形式が正しくありません',
            'email_confirm.max255'  => ':attributeは255字以内です',
            'email_confirm.same'  => ':attributeが一致していません',
        ],[
            'email' => 'メールアドレス',
            'email_confirm' => '確認用メールアドレス',
        ]);

        // 登録済みアドレスのチェック
        return Validator::make($data, [
            //'email' => ['required', 'string', 'email', 'max:255'],
            //'email_confirm' => ['required', 'string', 'email', 'max:255', 'same:email'],
            'email' => [ new EncryptEmail ],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Usermail
     */
    protected function create(array $data)
    {
        $app_key = config('app.key');
        // emailの暗号化
        $mid = Usermail::insertGetId([
            'email' => \DB::raw("HEX(AES_ENCRYPT('{$data['email']}', '{$app_key}'))"),
            'email_exp' => Carbon::now()->addMinutes(config('auth.verification.expire', 60)),
            'created_at' => Carbon::now(),
        ]);
        Log::debug(Usermail::find($mid));
        return Usermail::find($mid);
    }

    public function afterview()
    {
        return view('auth.register_send');
    }

}
