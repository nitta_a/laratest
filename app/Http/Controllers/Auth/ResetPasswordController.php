<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
//use App\Providers\AuthUserProvider;
//use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
//use App\Http\Controllers\Auth\PasswordBroker;
use Illuminate\Support\Str;
use App\User;
use Log;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;
    
    //protected $users;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    //public function __construct(UserProvider $users)
    //{
    //    $this->users = $users;
    //}

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    // オーバーライド
    public function showResetForm(Request $request, $token = null)
    {
        $credentials = $this->credentials($request);
        $user = $this->getEmail($credentials);

        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email, 'uid' => $user->uid]
        );
    }

    // 暗号化したメールアドレスの検索を行う
    public function getEmail(array $credentials)
    {

        // First we will add each credential element to the query as a where clause.
        // Then we can execute the query and, if we found a user, return it in a
        // Eloquent User "model" that will be utilized by the Guard instances.
        $user = new User;
        $model = $user->select();
        $query = DB::table($model,'users');
        foreach ($credentials as $key => $value) {
            if (Str::contains($key, 'password')) {
                continue;
            }

            if (is_array($value) || $value instanceof Arrayable) {
                $query->whereIn($key, $value);
            } else {
                $query->where($key, $value);
            }
        Log::debug($value);
        }
        // 論理削除、本登録以外を除外
        $query->where('removed', 0);
        $query->where('entry', 1);
        Log::debug($query->toSql());

        return $query->first();
    }


}
