<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\VerifiesEmails;
use App\Usermail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Log;

class EmailVerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        // 有効期限
        $this->middleware('signed')->only('verify');
        //$this->middleware('throttle:6,1')->only('verify', 'resend');
        Log::debug('__construct');

    }

    public function verify(Request $request)
    {
        // 有効なidか確認する
        $emailVerify = Usermail::find($request->route('id'));
        if (empty($emailVerify) || $emailVerify->hasVerifiedEmail()) {
            throw new AuthorizationException;
        }
        if (! hash_equals((string) $request->route('hash'), sha1($emailVerify->getEmailForVerification()))) {
            throw new AuthorizationException;
        }
        // 仮登録が済んでいる または 削除されたアドレスの判定
        if ($emailVerify->removed != 0 || $emailVerify->email_verified_at != null) {
            throw new AuthorizationException;
        }
        // ステータスをメール認証済みに変更する
        //$emailVerification->mailVerify();
//        $emailVerify->markEmailAsVerified();
        /*
        DB::beginTransaction();
        try {
            // DB更新
            $emailVerify->update([
                //'email_verified_at' => $this->freshTimestamp(),
                'email_verified_at' => NOW(),
            ]);
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::warning("メールアドレスの認証に失敗しました: email: {$emailVerify->email}", $e->getTrace());
            return redirect(route('/'))
                ->with(['message' => 'メールアドレスの認証に失敗しました。管理者にお問い合わせください。']);
        }
        DB::commit();
        */
        // 登録完了時にsessionにフラグを立てるため、初期化しておく
        $request->session()->forget('registflg');
        
        Log::debug(sha1($emailVerify->getEmailForVerification()));
        return view('auth.post_register')
            ->with(['mid' => $emailVerify->mid, 'hash' => $request->route('hash'), 'email' => $emailVerify->email]);
    }

}
