<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Usermail;
use App\Mail\EmailVerification;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Carbon;
use Log;

class PostRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/post_send';

    protected $request;
    protected $id;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guest');
        $this->request = $request;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // 確認画面を出力するため、こちらはダミー
        return Validator::make($data, []);
    }
    public function confirm()
    {
        //return Validator::make($data, [
        //Validator::make($data, [
        $this->validate($this->request, [
            'kname' => ['required', 'string', 'max:255'],
            //'yname' => ['required', 'string', 'max:255'],
            'sname' => ['required', 'string', 'max:255'],
            'tel' => ['required', 'string', 'max:20'],
            'fax' => ['required', 'string', 'max:20'],
            'zip' => ['required', 'string', 'max:8', 'min:7'],
            'address1' => ['required', 'string', 'max:80'],
            'address2' => ['required', 'string', 'max:80'],
            //'email' => ['required', 'string', 'email', 'max:255', 'unique:usermails'],
            //'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $param = $this->request->all();
        return view('auth.post_confirm', compact('param'));


    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $this->verify($this->request);

        //return User::create([
        $this->id = User::insertGetId([
            'usermails_mid' => $data['mid'],
            'kname' => $data['kname'],
            'yname' => $data['yname'],
            'sname' => $data['sname'],
            'license' => $data['license'],
            'tel' => $data['tel'],
            'fax' => $data['fax'],
            'zip1' => substr($data['zip'],0,3),
            'zip2' => substr($data['zip'],-4,4),
            'area' => $data['address1'],
            'address1' => config("area.address1.{$data['address1']}"),
            'address2' => $data['address2'],
            'address3' => $data['address3'],
            //'password' => Hash::make($data['password']),
            'created_at' => Carbon::now(),
        ]);
        return User::find($this->id);

    }
    public function verify($request)
    {
        // 有効なidか確認する
        $emailVerify = Usermail::find($request->mid);
        if (empty($emailVerify) || $emailVerify->hasVerifiedEmail()) {
            throw new AuthorizationException;
            Log::debug('emailVerify');
        }

        // ステータスをメール認証済みに変更する
        //$emailVerification->mailVerify();
        $emailVerify->markEmailAsVerified();

    }
    // 入力内容の確認画面表示用
/*
    public function confirm()
    {
        $data = $this->request->all();
        return view('auth.post_confirm', compact('data'));
    }
*/

    // オーバーライド
    public function register(Request $request)
    {
        if ($request->get('back')) {
            // 戻るボタンが押された場合
            $input = $request->except('back');
            $request->flash();
            //$refer = url()->previous();
            //return redirect()->action('Auth\EmailVerificationController@verify')->withInput($input);
            return view('auth.post_register')->with(['mid' => $request->mid, 'hash' => $request->hash, 'email' => $request->email]);
            //return redirect('post_register')->withInput($input);

        }else{
            $this->validator($request->all())->validate();

            if(!$this->request->session()->has('registflg')){
                event(new Registered($user = $this->create($request->all())));
                $request->session()->flash('userid', $this->id);

                //$this->guard()->login($user);

                //if ($response = $this->registered($request, $user)) {
                //    return $response;
                //}
                // ユーザーに控えメール
                $email = new EmailVerification($user,'pre');
                \Mail::to($user->email)->send($email);
                // 管理者（SPチーム）にメール
                $email = new EmailVerification($user,'copy');
                $to = config('mail.sp.address');
                \Mail::to($to)->send($email);
                // 仮登録完了フラグ
                $this->request->session()->put([
                    'registflg' => '1',
                ]);
            }

            return $request->wantsJson()
                        ? new Response('', 201)
                        : redirect($this->redirectPath());
        }
    }
    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
/*
    protected function registered(Request $request, $user)
    {
        //
    }
*/
    public function afterview()
    {
        $id = \Session::get('userid');
        $user = User::find($id);
        Log::debug($user);
        return view('auth.post_send')->with(['param' => $user]);
    }

    public function termsview()
    {
        return view('terms');
    }
}
