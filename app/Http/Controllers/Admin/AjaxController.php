<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Usermemo;
use Log;

class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $usermemo;
    protected $hinagata;
    protected $tachiai;

    public function __construct(Usermemo $usermemo)
    {
        $this->middleware('auth');
        $this->usermemo = $usermemo;
    }


    // ユーザーメモの編集用リスト取得
    public function getUsermemoList(Request $request)
    {
        $id = $request->users_id;
        $usermemo = DB::table('usermemo')->where('users_id',$id)->where('removed',0)->orderBy('num','desc')->get();
        return $usermemo;
    }
}
