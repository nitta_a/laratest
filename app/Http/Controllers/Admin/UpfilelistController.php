<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\User;
use App\Userupfile;
use Log;
use ZipArchive;

class UpfilelistController extends Controller
{
    protected $user;
    protected $request;
    protected $userupfile;

    public function __construct(User $user, Request $request, Userupfile $userupfile)
    {
        $this->middleware('auth:admin');
        $this->user = $user;
        $this->request = $request;
        $this->userupfile = $userupfile;
    }

    // 登録済み申込書を取得
    public function upfileList()
    {
        $app_key = \Config::get('app.key');

        $upfiles = $this->userupfile
                        ->select('userupfile.*',
                                 'users.kname','users.yname','users.sname','users.tel','users.fax',
                                 DB::raw('CONVERT(AES_DECRYPT(UNHEX(`usermails`.`email`), \''.$app_key.'\') USING utf8)  as email'))
                        ->join('users', 'userupfile.users_id', '=', 'users.id')
                        ->join('usermails',function ($join) {
                         $join->on('users.usermails_mid', '=', 'usermails.mid')
                              ->where('users.removed',0);
                              })
                       ->where( 'userupfile.removed', 0 )
                       ->orderBy( 'readflg','asc' )
                       ->orderBy( 'id','desc' )
                       ->get();
        return $upfiles;

    }
    public function upfileSearch()
    {
        $app_key = \Config::get('app.key');

        $query = $this->userupfile
                        ->select('userupfile.*',
                                 'users.kname','users.yname','users.sname','users.tel','users.fax',
                                 DB::raw('CONVERT(AES_DECRYPT(UNHEX(`usermails`.`email`), \''.$app_key.'\') USING utf8)  as email'))
                        ->join('users', 'userupfile.users_id', '=', 'users.id')
                        ->join('usermails',function ($join) {
                         $join->on('users.usermails_mid', '=', 'usermails.mid')
                              ->where('users.removed',0);
                              });
        // ファイル名
        if($this->request->original != ''){
            $original = trim($this->request->original);
            $original = "%".$original."%";
            //$query->where('users.kname','like',$kname);
            $query = $query->where(function($search) use($original)
            {
                $search->where('userupfile.original','like',$original);
                $search->orWhere('userupfile.original','like',$original);
            });
        }
        // 登録日
        if($this->request->rdate != ''){
            $query = $query->where('userupfile.rdate','=',$this->request->rdate);
        }
        // 電話番号
        if($this->request->tel != ''){
            $tel = str_replace("-","",$this->request->tel);
            $query = $query->whereRaw('REPLACE(users.tel,"-","") = ?',$tel);
        }
        // 処理状況
        if($this->request->readflg != ''){
            // 削除済みデータ
            if($this->request->readflg == "99"){
                $query = $query->where('userupfile.removed','<>', 0)
                       ->orderBy('userupfile.id','desc')->get();
                return $query;
            }else{
                $query = $query->where('userupfile.readflg','=',$this->request->readflg);
            }
        }

        $query = $query->where('userupfile.removed', 0)
                       ->orderBy('userupfile.id','desc')->get();
        return $query;

    }

    public function getFile($id)
    {
        $file = $this->userupfile
                       ->where( 'userupfile.id', $id )
                       ->where( 'userupfile.removed', 0 )
                       ->first();
        return $file;
    }
    public function index()
    {
        if (Auth::check()) {
            Log::debug("mode:".$this->request->mode);
            if($this->request->mode && $this->request->mode == 'flgchange'){
                if(isset($this->request->upflg)){
                    $res = $this->flgChange();
                    if($res){
                        $this->request->session()->flash('flashmessage','登録完了！');
                    }else{
                        $this->request->session()->flash('flashmessage','登録失敗！');
                    }
                    return redirect('admin/upfilelist');
                }
            }
            if($this->request->mode && $this->request->mode == 'search'){
                $upfiles = $this->upfileSearch();
            }else{
                $upfiles = $this->upfileList();
            }
            $input = $this->request->all();
            $this->request->session()->flash('_old_input', $input);
            //Log::debug(var_dump($upfiles));
            return view('admin.upfilelist', compact('upfiles'));
        } else {
            // ログインしていなかったら、Login画面を表示
            return view('admin.login');
        }
    }
    // ファイルダウンロード
    public function download()
    {
        $fileId = $this->request->fileId;
        $file = $this->getFile($fileId);
        //$filePath = "public/".$file->users_id."/".$file->rdate."/".$file->alias;
		$filePath = $file->alias;
		
        //$fileName = $file->alias;
		$fileName = $file->original;

        $mimeType = Storage::mimeType($filePath);

        $headers = [['Content-Type' => $mimeType]];

        return Storage::download($filePath, $fileName, $headers);
    }
    // 申込書の受付
    public function flgChange()
    {
        $upflg = $this->request->upflg;
        DB::beginTransaction();
        foreach($upflg as $value){
          Log::debug($value);
            try {
                $res = DB::table('userupfile')->where('id',$value)
                    ->update([
                      'readflg' => 1,
                      'up_uid' => Auth::user()->seq,
                      'up_user' => Auth::user()->ID,
                      'updated_at' => Carbon::now(),
                ]);
            } catch (\Exception $e) {
                Log::debug($e);
                DB::rollback();
                return;
            }
        }
        DB::commit();

        return $res;
    }
    // ファイルの削除
    public function fileDelete(Request $request)
    {
        $uid = Auth::user()->seq;
        $uname = Auth::user()->name;
        $fileId = $request->fileId;
        $file = $this->getFile($fileId);
        //$filePath = "public/".$file->users_id."/".$file->rdate."/".$file->alias;
		$filePath = $file->alias;

        //$fileName = $file->alias;
        DB::beginTransaction();
        try {
            $result = DB::table('userupfile')->where('id',$fileId )
                ->update(
                [
                    'up_uid' => $uid,
                    'up_user' => $uname,
                    'deldate' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'removed' => $fileId,
                ]
            );
            if(!$result){
                DB::rollback();
                // 接続エラー処理
                \Session::flash('flashmessage','ファイル削除が失敗しました。');
                Log::debug('ファイル削除が失敗しました。');
                return;
            }
            //ファイル削除
            Storage::delete($filePath);
        } catch (\Exception $e) {
            Log::debug($e);
            DB::rollback();
            \Session::flash('flashmessage','ファイル削除が失敗しました。');
            Log::debug('ファイル削除が失敗しました。');
            return;
        }
        DB::commit();
        return redirect('admin/upfilelist');

    }

    // ファイルの一括削除
    public function allfileDelete(Request $request)
    {
        $uid = Auth::user()->seq;
        $uname = Auth::user()->name;
        $delflg = $request->delflg;
        foreach((array)$delflg as $value){
            $file = $this->getFile($value);
            //$filePath = "public/".$file->users_id."/".$file->rdate."/".$file->alias;
			$filePath = $file->alias;

            //$fileName = $file->alias;
            DB::beginTransaction();
            try {
                $result = DB::table('userupfile')->where('id',$value )
                    ->update(
                    [
                        'up_uid' => $uid,
                        'up_user' => $uname,
                        'deldate' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'removed' => $value,
                    ]
                );
                if(!$result){
                    DB::rollback();
                    // 接続エラー処理
                    \Session::flash('flashmessage','ファイル削除が失敗しました。');
                    Log::debug('ファイル削除が失敗しました。');
                    return;
                }
                //ファイル削除
                Storage::delete($filePath);
            } catch (\Exception $e) {
                Log::debug($e);
                DB::rollback();
                \Session::flash('flashmessage','ファイル削除が失敗しました。');
                Log::debug('ファイル削除が失敗しました。');
                return;
            }
            DB::commit();
        }

        return redirect('admin/upfilelist');
    }

    public function zipDownload(Request $request) {

        $uid = Auth::user()->seq;
        $uname = Auth::user()->name;
        $list = $request->upflg;
        $zip = new ZipArchive(); //(2)
        $dir_path = storage_path("app\tmp");
        //$dir_path = public_path().'/storage/app/tmp/';
        $result = $zip->open($dir_path."\test2.zip", ZIPARCHIVE::CREATE);
        Log::debug($result);
        //if($zip->open(storage_path('/storage/app/tmp/test2.zip')) === true){

        foreach((array)$list as $value){
            $file = $this->getFile($value);
            $filePath = $file->alias;
            $file_name = $file->original;
            Log::debug($file_name);

            $zip->addFile($file, $file_name); 

        }
        $zip->close(); 
        //}


        return response()->download($dir_path.'test2.zip');

    }
}
