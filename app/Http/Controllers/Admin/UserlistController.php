<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
//use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder;
use App\Mail\EmailVerification;
use App\User;
use Log;

class UserlistController extends Controller
{
    protected $user;
    protected $request;

    public function __construct(User $user, Request $request)
    {
        $this->middleware('auth:admin');
        $this->user = $user;
        $this->request = $request;
    }
    //
    public function userList($id = null)
    {
        $query = $this->user->select();
        if($id != null){
            $users = DB::table($query,'users')->find($id);
                         
        }else{
            $users = DB::table($query,'users')->where('removed', 0 )->orderBy('id', 'desc')->get();
        }
        return $users;

    }
    // 検索
    public function userSearch()
    {
        $model = $this->user->select();
        $query = DB::table($model,'users');
        // 企業名 OR 屋号
        if($this->request->kname != ''){
            $kname = trim($this->request->kname);
            $kname = "%".$kname."%";
            //$query->where('users.kname','like',$kname);
            $query->where(function($search) use($kname)
            {
                $search->where('users.kname','like',$kname);
                $search->orWhere('users.yname','like',$kname);
            });
        Log::debug("kname=".$kname);
        Log::debug($query->toSql());
        }
        // 電話番号
        if($this->request->tel != ''){
            $tel = str_replace("-","",$this->request->tel);
            $query->whereRaw('REPLACE(users.tel,"-","") = ?',$tel);
        }
        // メールアドレス
        if($this->request->email != ''){
            $email = trim($this->request->email);
            $email = "%".$email."%";
            $query->where('users.email','like',$email);
        }
        // 1年以上アクセスなし
        if(isset($this->request->noaccess) && $this->request->noaccess == "1"){
            $lastyear = date("Y-m-d H:i:s",strtotime("-1 year"));
            $query->where('users.login_at','<',$lastyear);
        }
        // 登録状況
        if($this->request->entry != ''){
            // 削除済みデータ
            if($this->request->entry == "99"){
                $query = $query->where('users.removed','<>', 0)
                       ->orderBy('users.id','desc')->get();
                return $query;
            }else{
                $query = $query->where('users.entry','=',$this->request->entry);
                if($this->request->entry > 1){
                    $query = $query->orderBy('users.id','desc')->get();
                    return $query;
                }
            }
        }

        $query = $query->where('users.removed', 0)
                       ->orderBy('users.id','desc')->get();
        return $query;

    }

    public function errorCheck()
    {

    }

    public function mainRegistration()
    {

    }
    public function search()
    {
    }
    public function delete()
    {
    }
    // ユーザー本登録
    public function register()
    {
        $entry = $this->user->where('id',$this->request->userId)->first()->entry;
        if($entry == 1){
            $this->request->session()->flash('flashmessage','既に本登録済みです！');
        }else{
            // ユニークユーザーIDを作成（5回リトライで重複防止）
            for($i=0;$i<5;$i++){
                $uuid = $this->getUuid();
                $result = $this->user->where('uid',$uuid)->first();
                if(!$result){
                    $res = DB::table('users')->where('id',$this->request->userId)
                        ->update([
                          'entry' => 1,
                          'uid' => $uuid,
                          'password' => Hash::make($uuid),
                          'entry_user' => Auth::user()->ID,
                          'update_user' => Auth::user()->ID,
                          'entry_at' => Carbon::now(),
                          'updated_at' => Carbon::now(),
                    ]);
                    break;
                }
            }
            if($res){
                // ユーザーにメール
                $user = $this->user->find($this->request->userId);
                $email = new EmailVerification($user,'main');
                \Mail::to($user->email)->send($email);
                
                $this->request->session()->flash('flashmessage','登録完了！');
            }else{
                $this->request->session()->flash('flashmessage','登録失敗！');
            }
        }
        return $res;
    }
    // ユーザー否認
    public function userReject()
    {
        $entry = $this->user->where('id',$this->request->userId)->first()->entry;
        if($entry == 1){
            $this->request->session()->flash('flashmessage','既に本登録済みです！');
        }else{
            // 本登録フラグの更新
            DB::beginTransaction();
            try {
                $res = DB::table('users')->where('id',$this->request->userId)
                    ->update([
                      'removed' => $this->request->userId,
                      'entry' => 8,
                      'entry_user' => Auth::user()->ID,
                      'update_user' => Auth::user()->ID,
                      'entry_at' => Carbon::now(),
                      'updated_at' => Carbon::now(),
                      'deldate' => Carbon::now(),
                ]);
                // メールアドレス削除
                $usermails_mid = $this->user->where('id', $this->request->userId)->first()->usermails_mid;

                $result = DB::table('usermails')->where('mid',$usermails_mid )
                        ->update([
                          'removed' => $usermails_mid,
                          'update_user' => Auth::user()->ID,
                          'updated_at' => Carbon::now(),
                          'deldate' => Carbon::now(),
                ]);
                Log::debug("res:".$res);
                Log::debug("result:".$result);
                
                if(!$result){
                    // 接続エラー処理
                    Log::debug("useremails:removed失敗");
                    return;
                }
                if($res){
                    // ユーザーにメール
                    $user = $this->user->find($this->request->userId);
                    $email = new EmailVerification($user,'reject');
                    \Mail::to($user->email)->send($email);
                    
                    $this->request->session()->flash('flashmessage','ユーザーを否認しました！');
                }else{
                    $this->request->session()->flash('flashmessage','ユーザー否認に失敗しました！');
                    Log::debug("users:removed失敗");
                    return;
                }
            } catch (\Exception $e) {
                Log::debug($e);
                DB::rollback();
                return;
            }
            DB::commit();
        }
        return $res;
    }

    public function getUuid($length = 8)
    {
        // ランダムなユーザーID（8桁）を作成する
        return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, $length);
    }

    public function index()
    {
        Log::debug("mode1:".$this->request->mode);
        // ログインしていたら、一覧を表示
        if (Auth::check()) {
            if($this->request->mode && $this->request->mode != 'search'){
                if( $this->request->mode == 'register'){
                    $res = $this->register();
                }else if( $this->request->mode == 'reject'){
                    $res = $this->userReject();
                }else if( $this->request->mode == 'delete'){
                    $res = $this->userDelete();
                }else if( $this->request->mode == 'erase'){
                    $res = $this->userErase();
                }else if( $this->request->mode == 'update_all'){
                    $res = $this->update_all();
                }else if($this->request->mode == 'memoadd'){
                    $res = $this->usermemoAdd();
                }else if($this->request->mode == 'memoupd'){
                    $res = $this->usermemoUpd();
                }else if($this->request->mode == 'memodel'){
                    $res = $this->usermemoDel();
                }
                if(!$res && !$this->request->session()->has('flashmessage')){
                    $this->request->session()->flash('flashmessage','登録失敗！');
                }
                return redirect('/admin/userlist');
            }
            
            // ユーザー情報
            //$users = $this->userList();
            if($this->request->mode && $this->request->mode == 'search'){
                $users = $this->userSearch();
            }else{
                $users = $this->userList();
            }
            //Log::debug("memo:".$usermemo);
            // ユーザーメモ
            $usermemo = $this->usermemoList($users);
            $input = $this->request->all();
            $this->request->session()->flash('_old_input', $input);
            $old = $this->request->session()->all();
            Log::debug($old);

            return view('admin/userlist', compact('users','usermemo'));
            //return view('admin/newadd');
        } else {
            // ログインしていなかったら、Login画面を表示
            return view('admin.login');
        }
    }
    // ユーザーメモを取得
    public function usermemoList($users)
    {
        //if(!is_array($users)) return;
        $usermemo = array();
        foreach($users as $value){
            $usermemo[$value->id] = DB::table( 'usermemo' )
                                      ->where( 'users_id',$value->id )
                                      ->where( 'usermemo.removed', 0)
                                      ->orderBy( 'num','desc' )
                                      ->get();
        }
        return $usermemo;

    }

    // ユーザーメモを登録
    public function usermemoAdd()
    {
        $today = date('Y-m-d');
        Log::debug("userId:".$this->request->userId);
        Log::debug("memo:".$this->request->usermemo[$this->request->userId]);

        DB::beginTransaction();
        try {
            $res = DB::table('usermemo')->insert(
                  [
                   'users_id' => $this->request->userId,
                   'num' => DB::table('usermemo')->select(DB::raw('ifnull(max(num)+1,1) as num'))
                             ->where( 'users_id', $this->request->userId )->first()->num,
                   'rdate' => $today,
                   't_uid' => Auth::id(),
                   't_user' => Auth::user()->name,
                   'memo' => $this->request->usermemo[$this->request->userId],
                   'created_at' => NOW()
                  ]
            );
            if($res){
                $this->request->session()->flash('flashmessage','登録完了！');
            }else{
                $this->request->session()->flash('flashmessage','登録失敗！');
            }
        } catch (\Exception $e) {
            Log::debug($e);
            DB::rollback();
            return;
        }
        //Log::debug($res->toSql());
        DB::commit();

        // 二重送信対策
        //$this->request->session()->regenerateToken();  //これを入れるとログアウトする

        return $res;

    }

    // ユーザーメモを編集
    public function usermemoUpd()
    {
            $res = DB::table('usermemo')->where('id',$this->request->memo_id)
                  ->update(
                  [
                   'up_uid' => Auth::id(),
                   'up_user' => Auth::user()->name,
                   'memo' => $this->request->upusermemo[$this->request->memo_id],
                   'updated_at' => NOW()
                  ]
               );
            if($res){
                $this->request->session()->flash('flashmessage','編集完了！');
            }else{
                $this->request->session()->flash('flashmessage','編集失敗！');
            }
            return $res;

    }

    // ユーザーメモを削除
    public function usermemoDel()
    {
        $today = date('Y-m-d');

        $users_id = DB::table('usermemo')->select('users_id')->where('id',$this->request->memo_id)->first()->users_id;
        Log::debug($users_id);

        DB::beginTransaction();
        try {
            $res = DB::table('usermemo')->where('id',$this->request->memo_id)
                  ->update(
                  [
                   'deldate' => $today,
                   'up_uid' => Auth::id(),
                   'up_user' => Auth::user()->name,
                   'removed' => $this->request->memo_id,
                   'updated_at' => Carbon::now()
                  ]
               );
                if($res){
                    $this->request->session()->flash('flashmessage','備考を削除しました。');
                }else{
                    $this->request->session()->flash('flashmessage','備考の削除に失敗しました。');
                }
        } catch (\Exception $e) {
            Log::debug($e);
            DB::rollback();
            return;
        }
        DB::commit();

        return $res;

    }
    // ユーザー論理削除（停止）
    public function userDelete()
    {
        $id = $this->request->userId;
        $today = date('Y-m-d');
        $up_user = Auth::id();
        DB::beginTransaction();
        try {
            $result = DB::table('users')->where('id',$id )
                ->update(
                [
                  'removed' => $id,
                  'entry' => 2,
                  'update_user' => $up_user,
                  'updated_at' => Carbon::now(),
                  'deldate' => Carbon::now(),
                ]
            );
            if(!$result){
                // 接続エラー処理
                Log::debug("users:removed失敗");
                $this->request->session()->flash('flashmessage','ユーザーの停止に失敗しました。');
                DB::rollback();
                return;
            }

            $usermails_mid = $this->user->where('id', $id)->first()->usermails_mid;

            $result = DB::table('usermails')->where('mid',$usermails_mid )
                ->update(
                [
                  'removed' => $usermails_mid,
                  'update_user' => $up_user,
                  'updated_at' => Carbon::now(),
                  'deldate' => Carbon::now(),
                ]
            );
            if(!$result){
                // 接続エラー処理
                Log::debug("useremails:removed失敗");
                $this->request->session()->flash('flashmessage','ユーザーの停止に失敗しました。');
                return;
            }else{
                $this->request->session()->flash('flashmessage','ユーザーを停止しました');
            }
        } catch (\Exception $e) {
            Log::debug($e);
            DB::rollback();
            return;
        }
        DB::commit();

        return $result;
    }
    // ユーザー物理削除（抹消）
    public function userErase()
    {
        $id = $this->request->userId;
        $today = date('Y-m-d');
        $up_user = Auth::id();
        DB::beginTransaction();
        try {
            // 削除ログを記録する
            $res = DB::table('delete_log')->insert(
                  [
                   'users_id' => $id,
                   'ipaddr' => $this->request->ip(),
                   'host' => $this->request->getHttpHost(),
                   'useragent' => $this->request->userAgent(),
                   'created_user' => $up_user,
                   'created_at' => Carbon::now()
                  ]
            );
            if(!$res){
                Log::debug('削除ログテーブルへの挿入に失敗しました');
                $this->request->session()->flash('flashmessage','削除ログテーブルへの挿入に失敗しました');
                DB::rollback();
                return;
            }
            // メールアドレス削除
            $usermails_mid = $this->user->where('id', $id)->first()->usermails_mid;

            $result = DB::table('usermails')->where('mid',$usermails_mid )->delete();
            if(!$result){
                // 接続エラー処理
                $this->request->session()->flash('flashmessage','削除に失敗しました');
                Log::debug("useremails:removed失敗");
                return;
            }

            // ユーザー情報削除
            $result = DB::table('users')->where('id',$id )->delete();
            if(!$result){
                // 接続エラー処理
                Log::debug("users:delete失敗");
                $this->request->session()->flash('flashmessage','削除に失敗しました');
                DB::rollback();
                return;
            }
            // アップロードファイル登録テーブル削除
            $count = DB::table('userupfile')->selectRaw('count(*) as cnt')->where('users_id',$id)->first()->cnt;
            if($count > 0){
                $result = DB::table('userupfile')->where('users_id',$id)->delete();
                if(!$result){
                    // 接続エラー処理
                    Log::debug("userupfile:delete失敗");
                    DB::rollback();
                    return;
                }else{
                    // ファイル保存ディレクトリ削除
                    Storage::deleteDirectory('tmp/'.$id);
                    Storage::deleteDirectory('public/'.$id);
                }
            }
        } catch (\Exception $e) {
            Log::debug($e);
            //DB::rollback();
            return;
        }
        DB::commit();
        $this->request->session()->flash('flashmessage','削除完了！');
        Log::debug("削除完了");

        return $result;
    }


}
