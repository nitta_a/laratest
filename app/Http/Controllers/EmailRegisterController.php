<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Usermail;
use App\Rules\EncryptEmail;
//use App\Notifications\VerifyEmailJP;
//use Illuminate\Notifications\Notifiable;
//use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Validation\Rule;
use Illuminate\Support\Carbon;
use Log;

class EmailRegisterController extends Controller 
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    protected $request;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/changemail_send';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
/*
    public function __construct(Request $request)
    {
        //$this->middleware('guest');
        //$this->middleware(['auth']);
        //$this->middleware(['auth','verified']);
        $this->request = $request;
    }
*/
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // emailと確認用の入力チェック（順番考慮）
        $this->validate($this->request, [
            'email' => 'required|email|max:255',
            'email_confirm' => 'required|email|max:255|same:email',
        ],[
            'email_confirm.required' => ':attributeは必須です',
            'email_confirm.email' => ':attribute形式が正しくありません',
            'email_confirm.max255'  => ':attributeは255字以内です',
            'email_confirm.same'  => ':attributeが一致していません',
        ],[
            'email_confirm' => '確認用メールアドレス',
        ]);

        // 登録済みアドレスのチェック
        return Validator::make($data, [
            //'email' => ['required', 'string', 'email', 'max:255'],
            //'email_confirm' => ['required', 'string', 'email', 'max:255', 'same:email'],
            'email' => [ new EncryptEmail ],
        ]);
    }
    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // オーバーライド
    public function register(Request $request)
    {
        $this->request = $request;
        $this->validator($request->all())->validate();

        //event(new Registered($user = $this->create($request->all())));
        $user = $this->create($request->all());
        $user->sendEmailResetNotification();

        //$this->guard()->login($user);

        //if ($response = $this->registered($request, $user)) {
        //    return $response;
        //}

        return $request->wantsJson()
                    ? new Response('', 201)
                    : redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Usermail
     */
    protected function create(array $data)
    {
        $app_key = config('app.key');
        // emailの暗号化
        $mid = Usermail::insertGetId([
            'email' => \DB::raw("HEX(AES_ENCRYPT('{$data['email']}', '{$app_key}'))"),
            'email_exp' => Carbon::now()->addMinutes(config('auth.verification.expire', 60)),
            'changemid' => $this->request->mid,
            'created_at' => Carbon::now(),
        ]);
        Log::debug(Usermail::find($mid));
        return Usermail::find($mid);
    }

    public function afterview()
    {
        return view('changemail_send');
    }
    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
        //$this->toMail($user);
    }

/*
    // オーバーライド
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailJP('changemail'));
    }

    // オーバーライド

    public function toMail($notifiable)
    {
        $verificationUrl = $this->verificationUrl($notifiable);

        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $verificationUrl);
        }

        return (new MailMessage)
            ->subject(Lang::get('mail.changemail.subject'))
            ->line(Lang::get('mail.changemail.line_01'))
            ->action(Lang::get('mail.changemail.action'), $verificationUrl)
            ->line(Lang::get('mail.changemail.line_02'));
    }
*/
}
