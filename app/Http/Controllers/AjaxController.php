<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\User;
use App\Inquiry;
use App\Inquirydetail;
//use App\Bukken;
use App\Userupfile;
use Log;

use Auth;

class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $user;
    protected $inquiry;
    protected $inqdetail;
    //protected $bukken;
    protected $userupfile;

    //public function __construct(User $user, Inquiry $inquiry, Inquirydetail $inqdetail, Bukken $bukken, Userupfile $userupfile)
	public function __construct(User $user, Inquiry $inquiry, Inquirydetail $inqdetail, Userupfile $userupfile)
    {
        $this->middleware(['auth','verified']);
        $this->user = $user;
        $this->inquiry = $inquiry;
        $this->inqdetail = $inqdetail;
        //$this->bukken = $bukken;
        $this->userupfile = $userupfile;
    }
    // 入力内容の確認画面表示用
    public function confirm(Request $request)
    {
        $this->validate($request, [
            'staff' => 'required|string|max:255',
            'tel'  => 'required|string|max:255',
        ]);
        Log::debug($errors);
    }
    public function errorCheck(Request $request)
    {

        $errormsg = array();
        $staff = trim($request->staff);
        $tel = trim($request->tel);
        if($staff == ""){
            array_push($errormsg, 'ご担当者名は必須です' );
        }
        if($tel == ""){
            array_push($errormsg, '電話番号は必須です' );
        }
        return $errormsg;
    }


    // 問い合わせ記録
    public function regist(Request $request)
    {
        //$this->confirm($request);
        
        $errormsg = $this->errorCheck($request);
        if(count($errormsg) == 0){
            DB::beginTransaction();
            try {
                $inquiry = [
                    'acceptdate' => Carbon::now(),
                    'kname' => Auth::user()->kname,
                    'tel' => $request->tel,
                    'uid' => Auth::user()->id,
                    'staff'  => $request->staff,
                    //'support'  => $request->support,
                    'created_at' => Carbon::now(),
                    //'created_user'  => $request->uid,
					'created_user'  => Auth::user()->uid
                ];
                $id = $this->inquiry->insertGetId($inquiry);
                if(!$id){
                    \Session::flash('flashmessage','登録が失敗しました。');
                    Log::debug('登録が失敗しました。');
                }else{
					foreach ((array)$request->bseq as $i => $v) {
						$inqdetail = [
							'inq_id' => $id,
							'bseq' => $v,
							'rseq' => $request->rseq[$i],
							//'rtype' => $request->rtype,
							'guide'  => 1,
							//'remarks'  => $request->remarks,
							//'support'  => $request->support,
							'created_at' => Carbon::now(),
							//'created_user'  => $request->uid,
							'created_user'  => Auth::user()->uid
						];
						
						$result = $this->inqdetail->insertGetId($inqdetail);
						if(!$result){
							\Session::flash('flashmessage','登録が失敗しました。');
							Log::debug('登録が失敗しました。');
							return;
						}
					}
                    $complete = 1;
                }
            } catch (\Exception $e) {
                Log::debug($e);
                DB::rollback();
                return;
            }
            DB::commit();
            
            return $this->getGuide($request);
        }else{
            return json_encode(["errors" => $errormsg]);
        }
    }

    // 内覧方法取得
    public function getGuide(Request $request)
    {
		$table = [];
		$col = [
			"bseq",
			"rseq",
			"shubetu",
			"bname",
			"rno",
			"alno",
			"jno1",
			"jno2",
			"tno1",
			"tno2",
			"evno"
		];
		foreach ((array)$request->rseq as $i => $v) {
			$table[] = DB::table('vacancy')->select($col)->where('rseq', $v)->first();
		}
		
		// 重複削除
		$tmp = [];
		$guide = [];
		foreach ($table as $r){
		   if (!in_array($r->bseq, $tmp)) {
			  $tmp[] = $r->bseq;
			  $guide[] = $r;
		   }
		}
		
        //$bseq = $request->bseq;
        //$guide = $this->bukken->where('物件SEQ',$bseq)->first();
        Log::debug($guide);
        return $guide;
    }
    // 申込書アップロード（ドラッグドロップ）
    public function fileUpload(Request $request)
    {
        //一時ディレクトリ作成
        Storage::makeDirectory('tmp');
		
		$upfilesize = config('setting.upfilesize');
		$extensionlist = config('setting.extensionlist');
		
        $uid = Auth::user()->id;
        $file = $request->file('file');
		$size = $file->getSize();
		$ext = mb_strtolower($file->getClientOriginalExtension());
        //$file_name = $file->getClientOriginalName();

        //ユーザーディレクトリ作成
        Storage::makeDirectory('tmp/'.$uid);
		
		if ($upfilesize < $size) {
			return [
				"result" => false,
				"message" => "アップロード可能なファイルサイズを超えています。"
			];
		}
		if (!in_array($ext, $extensionlist)) {
			return [
				"result" => false,
				"message" => "アップロード可能なファイル形式ではありません。"
			];
		}
		
		$hashname = $file->store('tmp/'.$uid);
		
        //$request->file('file')->storeAs('tmp/'.$uid, $file_name);
        /*foreach($files as $file){
            $file_name = $file->getClientOriginalName();
            $file->store('');

        }*/
        //move_uploaded_file( $_FILES['file']['tmp_name'], '../storage/tmp/'.$_FILES['file']['name'] );
        //Log::debug(dd($request->all()));
		return [
			"result" => true,
			"hashname" => $hashname
		];
    }
    // 申込書アップロード（保存）
	// ！！！！！未使用！！！！！
    public function fileSave(Request $request)
    {
//        $today = date('Y-m-d');
//        $files = $request->filename;
//        $uid = Auth::user()->id;
//        DB::beginTransaction();
//        try {
//            foreach($files as $key => $value){
//                $upfile = [
//                    'rdate' => Carbon::now(),
//                    'user_id' => $uid,
//                    'original' => $value,
//                    'alias' => $value,
//                    'created_at' => Carbon::now(),
//                    'created_user'  => $request->uid,
//                ];
//                $id = $this->userupfile->insertGetId($upfile);
//                if(!$id){
//                    \Session::flash('flashmessage','登録が失敗しました。');
//                    Log::debug('登録が失敗しました。');
//                }else{
//                    //ユーザーディレクトリ作成
//                    Storage::makeDirectory('public/'.$uid);
//
//                    //日付フォルダ作成
//                    Storage::makeDirectory('public/'.$uid."/".$today);
//
//                    //tmpフォルダからユーザーフォルダへ移動
//                    //Storage::move('tmp/'.$uid."/".$value, 'public/'.$uid."/".$today."/".$value);
//                    $path = Storage::putFile('public/'.$uid."/".$today, 'tmp/'.$uid."/".$value);
//                    Log::debug('path='.$path);
//                }
//            }
//        } catch (\Exception $e) {
//            Log::debug($e);
//            DB::rollback();
//            return;
//        }
//        DB::commit();
//        
    }
}