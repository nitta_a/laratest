<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\VerifiesEmails;
use App\Usermail;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Log;

class EmailVerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
        //$this->middleware(['auth','verified']);
        // 有効期限
        $this->middleware('signed')->only('verify');
        //$this->middleware('throttle:6,1')->only('verify', 'resend');

    }

    public function verify(Request $request)
    {
        // 有効なidか確認する
        $emailVerify = Usermail::find($request->route('id'));
        if (empty($emailVerify) || $emailVerify->hasVerifiedEmail()) {
            throw new AuthorizationException;
        }
        if (! hash_equals((string) $request->route('hash'), sha1($emailVerify->getEmailForVerification()))) {
            throw new AuthorizationException;
        }
        $userid = User::where('usermails_mid',$emailVerify->changemid)
                      ->where('users.removed',0)->first();
        Log::debug($userid);
        if(!$userid || !$userid->id){
            throw new AuthorizationException;
        }
        $old_email = Usermail::find($emailVerify->changemid);

        // ステータスをメール認証済みに変更する
        //$emailVerification->mailVerify();
//        $emailVerify->markEmailAsVerified();
        
        DB::beginTransaction();
        try {
            // DB更新
            // 変更前のアドレスを論理削除
            $res = $old_email->forceFill([
                'removed' => $emailVerify->changemid,
                'updated_at' => Carbon::now(),
                'deldate' => Carbon::now(),
            ])->save();
            if(!$res){
                throw new AuthorizationException;
            }
            // 変更後のアドレスを認証
            $res = $emailVerify->forceFill([
                //'email_verified_at' => $this->freshTimestamp(),
                'email_verified_at' => Carbon::now(),
            ])->save();
            if(!$res){
                throw new AuthorizationException;
            }
            // usersテーブルのメールidを更新
            $res = $userid->update([
                'usermails_mid' => $request->route('id'),
                'updated_at' => Carbon::now(),
                'update_user' => $userid->uid,
            ]);
            if(!$res){
                throw new AuthorizationException;
            }
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::warning("メールアドレスの認証に失敗しました: email: {$emailVerify->email}", $e->getTrace());
            return redirect(route('/'))
                ->with(['message' => 'メールアドレスの認証に失敗しました。管理者にお問い合わせください。']);
        }
        DB::commit();
        
        Log::debug(sha1($emailVerify->getEmailForVerification()));
        return view('changemail_close')
            ->with(['old_email' => $old_email->email, 'new_email' => $emailVerify->email]);
    }
    public function afterview()
    {
        return view('changemail_close');
    }

}
