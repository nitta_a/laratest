<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
// 日本語のメールを作成するクラス
use App\Notifications\VerifyEmailJP;
use App\Notifications\ResetPasswordJP as ResetPasswordNotificationJP;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usermails_mid', 'uid', 'password', 'entry', 'entry_at', 'entry_user', 'kname', 'yname', 'sname', 'license', 'tel', 'fax', 
        'zip1', 'zip2', 'area', 'address1', 'address2', 'address3', 'created_user', 'update_user', 'deldate', 'email','removed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'entry_at' => 'datetime',
    ];

    protected static function booted()
    {

        static::addGlobalScope('users', function (Builder $builder) {
        $app_key = \Config::get('app.key');

            $builder->join('usermails','usermails.mid','users.usermails_mid')
                    ->select('users.*',DB::raw('CONVERT(AES_DECRYPT(UNHEX(`usermails`.`email`), \''.$app_key.'\') USING utf8)  as email'));
        });

    }

    // ログインアカウントに使用するカラム
    public function username()
    {
        return 'uid';
    }
/*
    public function getEmailForPasswordReset()
    {
        $app_key = config('app.key');
         
        return $this->join('usermails','usermails.mid','users.usermails_mid')->select(DB::raw('CONVERT(AES_DECRYPT(UNHEX(`usermails`.`email`), \''.$app_key.'\') USING utf8)  as email'))
            ->where('mid',$id);
    }
*/
    // オーバーライド
    public function sendPasswordResetNotification($token)
    {
        // $this->notify(new ResetPasswordNotification($token));
        $this->notify(new ResetPasswordNotificationJP($token));
    }

    // オーバーライド
    public function sendEmailVerificationNotification()
    {
        // $this->notify(new VerifyEmail);
        $this->notify(new VerifyEmailJP);
    }
 
}
