<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;
    
    protected $user;
    protected $mode;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$mode)
    {
        //
        $this->user = $user;
        $this->mode = $mode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        if($this->mode == 'pre'){
            $this->previous();
        }else if($this->mode == 'main'){
            $this->finish();
        }else if($this->mode == 'reject'){
            $this->reject();
        }else if($this->mode == 'copy'){
            $this->copymail();
        }
    }
    public function previous()
    {
        //return $this->view('view.name');
        return $this
            ->subject('仮登録の完了')
//            ->view('auth.email.pre_register')
//            ->with(['user' => $this->user,]);
			->markdown('vendor.notifications.email')
			->with(
				[
					'greeting' => '仮登録の完了',
					'customLines' => [
						$this->user->kname." 様\n",
						"この度は、".config('app.sname')."に会員登録依頼をいただきまして、有り難うございます。\n現在は仮登録の状態です。",
						"登録情報の審査後にID・PWが発行され、ご登録のメールアドレス宛に通知されますので今しばらくお待ちください。",
						"ご入力内容は以下の通りです。"
					],
					// email.blade.php内で$tableをそのまま出力しているのでエスケープ処理注意
					'table' => ""
						. "|  |  |\n"
						. "| :---- | :---- |\n"
						. "| 企業名 | ".e($this->user->kname)." |\n"
						. "| 屋号 | ".e($this->user->yname)." |\n"
						. "| 支店名 | ".e($this->user->sname)." |\n"
						. "| 宅建免許番号 | ".e($this->user->license)." |\n"
						. "| 住所 | 〒".e($this->user->zip1."-".$this->user->zip2)."<br>"
									. "".e($this->user->address1.$this->user->address2.$this->user->address3)." |\n"
						. "| 電話番号 | ".e($this->user->tel)." |\n"
						. "| FAX番号 | ".e($this->user->fax)." |\n"
				]
			);
    }
    public function finish()
    {
        //return $this->view('view.name');
        return $this
            ->subject('本登録の完了')
//            ->view('auth.email.main_register')
//            ->with(['user' => $this->user,]);
			->markdown('vendor.notifications.email')
			->with(
				[
					'greeting' => '本登録の完了',
					'customLines' => [
						$this->user->kname." 様\n",
						"この度は、".config('app.sname')."に会員登録依頼をいただきまして、有り難うございます。\n"
						. "本登録が完了し、ID・PWが発行されましたので以下の通りご案内いたします。\n",
						"## ID：".$this->user->uid,
						"## PW：初回ログイン時はIDと同じ",
						"パスワードについては初回ログイン時に変更画面が表示されますので、任意のパスワードに変更してください。"
					],
					'actionText' => 'ログイン画面へ',
					'actionUrl' => route('login')
				]
			);
    }
    public function reject()
    {
        //return $this->view('view.name');
        return $this
            ->subject('本登録が否認されました')
//            ->view('auth.email.main_reject')
//            ->with(['user' => $this->user,]);
			->markdown('vendor.notifications.email')
			->with(
				[
					'greeting' => '本登録が否認されました',
					'customLines' => [
						$this->user->kname." 様\n",
						"この度、".config('app.sname')."に会員登録依頼をいただきましたが、登録内容の審査の結果、本登録が否認されました。",
						"詳細についてご案内が必要な場合は個別に対応させていただきますので、お手数ではございますが、お電話にてお問い合わせください。"
					]
				]
			);
    }
    public function copymail()
    {
        //return $this->view('view.name');
        return $this
            ->subject('【'.config('app.sname').'】仮登録が送信されました')
//            ->view('auth.email.copy_register')
//            ->with(['user' => $this->user,]);
			->markdown('vendor.notifications.email')
			->with(
				[
					'greeting' => '仮登録が送信されました',
					'customLines' => [
						$this->user->kname." 様より".config('app.sname')."への会員登録依頼がありました。",
						"現在は仮登録の状態ですので、登録内容を審査の上、管理機能のユーザー一覧画面より「登録」または「否認」を決定してください。",
						"[ユーザー一覧画面へ](".route('admin.userlist').")"
					]
				]
			);
    }

}
