<?php

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider;

class AuthUserProvider extends EloquentUserProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    public function retrieveById($identifier) {
        $result = $this->createModel()->newQuery()
            ->leftJoin('useremails', 'users.useremails_id', '=', 'useremails.id')
            ->select(['users.*', 'useremails.email as email'])
            ->find($identifier);
        return $result;
    }
}
