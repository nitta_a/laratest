<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Usermail;
use Log;

class EncryptEmail implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // すでに登録済みのアドレスチェック
        $app_key = config('app.key');

        $result = DB::select('SELECT `mid`, `decryptmail`, `email_exp`, `email_verified_at` FROM (
                  SELECT `mid`, CONVERT(AES_DECRYPT(UNHEX(`usermails`.`email`),'."'".$app_key."'".') USING utf8) AS `decryptmail`, `email_exp`, `email_verified_at`
                  FROM usermails
                  WHERE removed=0) AS a WHERE decryptmail='."'".$value."'");
        if(count($result) === 0){
            return 1;
        // 登録があるが認証がなく期限切れの場合、削除する
        }else if($result[0]->email_verified_at == null && Carbon::parse($result[0]->email_exp) < Carbon::now()){
            $res = DB::table('usermails')->where('mid',$result[0]->mid)
                  ->update(
                  [
                   'deldate' => Carbon::now(),
                   'removed' => $result[0]->mid,
                   'updated_at' => Carbon::now()
                  ]
               );
            if($res){
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }

        //return ($result[0]->AGGREGATE === 0);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'すでに登録済みのアドレスです。';
    }
}
