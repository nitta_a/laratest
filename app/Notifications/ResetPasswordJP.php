<?php


namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

// オーバーライドに必要
use Illuminate\Support\Facades\Lang;

class ResetPasswordJP extends ResetPassword
{
    // オーバーライド
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }
        if (static::$createUrlCallback) {
            $url = call_user_func(static::$createUrlCallback, $notifiable, $this->token);
        } else {
            $url = url(route('password.reset', [
                'token' => $this->token,
                'email' => $notifiable->getEmailForPasswordReset(),
            ], false));
        }

           // ->action(Lang::get('mail.password_reset.action'), url(config('app.url').route('password.reset', ['token' => $this->token, 'email' => $notifiable->getEmailForPasswordReset()], false)))
        return (new MailMessage)
            ->subject(Lang::get('mail.password_reset.subject'))
			->greeting(Lang::get('mail.password_reset.greeting'))
            ->line(Lang::get('mail.password_reset.line_01'))
			->line(Lang::get('mail.password_reset.line_02', ['count' => config('auth.passwords.'.config('auth.defaults.passwords').'.expire')]))
            ->action(Lang::get('mail.password_reset.action'), $url);
    }

}
