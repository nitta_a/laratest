<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
// 日本語のメールを作成するクラス
use App\Notifications\VerifyEmailJP;
use App\Notifications\ChangeVerifyEmailJP;
use App\Notifications\ResetPasswordJP as ResetPasswordNotificationJP;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

use Log;

class Usermail extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    protected $primaryKey = 'mid';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','changemid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
/*
    public function hasVerifiedEmail()
    {
        return ! is_null($this->email_verified_at);
        //return ! is_null($this->uid);
    }
*/
    // オーバーライド
    public function sendEmailVerificationNotification()
    {
        // $this->notify(new VerifyEmail);
        $this->notify(new VerifyEmailJP);
    }
 
    // オーバーライド
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotificationJP($token));
    }

    public function sendEmailResetNotification()
    {
        $this->notify(new ChangeVerifyEmailJP);
    }

/*
    public function routeNotificationFor($driver, $notification = null)
    {
            Log::debug('routeNotificationFor_1');

        if (method_exists($this, $method = 'routeNotificationFor'.Str::studly($driver))) {
            return $this->{$method}($notification);
        }
            Log::debug('routeNotificationFor_2');
            Log::debug($this->emaildecrypt);
            Log::debug($this->email);

        switch ($driver) {
            case 'database':
                return $this->notifications();
            case 'mail':
                //return $this->email;
                return $this->emaildecrypt;
        }
    }
*/

    public function getEmailDecryptAttribute($id)
    {
        $app_key = config('app.key');
        // 複数クエリーに気をつけてください
        return $this->select('mid',DB::raw('CONVERT(AES_DECRYPT(UNHEX(`email`), \''.$app_key.'\') USING utf8)  as email'))
            ->where('mid',$id);
    }
    protected static function booted()
    {

        static::addGlobalScope('email', function (Builder $builder) {
            $app_key = config('app.key');
            $builder->select('mid',DB::raw('CONVERT(AES_DECRYPT(UNHEX(`email`), \''.$app_key.'\') USING utf8)  as email'),'changemid','email_verified_at','removed');
        });
    }

}
