<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 
class Inquirydetail extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'inquiry_detail';

    protected $fillable = [
       'inq_id', 'did', 'bseq', 'rseq', 'rtype', 'guide', 'remarks', 'support', 'update_user', 'create_at', 'create_user', 'update_at', 'deldate',

    ];


}
