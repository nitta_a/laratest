<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 
class Inquiry extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'inquiry';

    protected $fillable = [
       'id', 'acceptdate', 'tel', 'kanme', 'uid', 'staff', 'support', 'update_user', 'create_at', 'create_user', 'update_at', 'deldate',

    ];


}
