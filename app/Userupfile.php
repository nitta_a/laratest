<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
 
class Userupfile extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'userupfile';

    protected $fillable = [
        'id','users_id','num', 'rdate', 't_uid', 't_user', 'original', 'alias', 'size', 'readflg','created_at', 'deldate', 'removed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
/*
    protected $hidden = [
        'password', 'remember_token',
    ];
*/
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
/*
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
*/

}
